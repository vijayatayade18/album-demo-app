import 'package:album/constants/index.dart';
import 'package:album/frameworks/app_theme/index.dart';
import 'package:flutter/material.dart';

class AppMultiLineEditTextTheme extends MultiLineEditTextTheme {
  TextStyle enabledLabelStyle;
  TextStyle enabledPlaceHolderStyle;
  TextStyle enabledTextStyle;
  TextStyle disableLabelStyle;
  TextStyle disablePlaceHolderStyle;
  TextStyle disableTextStyle;
  Color focusColor;
  Color normalColor;
  Color errorColor;
  Color backgroundColor;
  Color disableColor;
  Color enabledColor;
  Color cursorColor;

  AppMultiLineEditTextTheme(
      {this.enabledLabelStyle = TextStyles.inputFieldLabel,
      this.enabledPlaceHolderStyle = TextStyles.inputFieldPlaceHolder,
      this.enabledTextStyle = TextStyles.inputFieldText,
      this.disableLabelStyle = TextStyles.disableInputFieldLabel,
      this.disablePlaceHolderStyle = TextStyles.disableInputFieldPlaceHolder,
      this.disableTextStyle = TextStyles.disableInputFieldText,
      this.focusColor = AppColors.white,
      this.normalColor = AppColors.white,
      this.errorColor = AppColors.red,
      this.backgroundColor = Colors.transparent,
      this.disableColor = AppColors.white,
      this.enabledColor = AppColors.white,
      this.cursorColor = AppColors.white});
}

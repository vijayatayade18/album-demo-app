import 'package:album/frameworks/app_theme/index.dart';
import 'package:flutter/material.dart';


class ThemeProvider extends InheritedWidget {
  final AppThemeData theme;

  ThemeProvider({Key key, this.theme, Widget child})
      : super(key: key, child: child);

  bool updateShouldNotify(_) => true;

  static AppThemeData of(BuildContext context) {
    return context.dependOnInheritedWidgetOfExactType<ThemeProvider>().theme;
  }
}

import 'package:album/constants/app_colors.dart';
import 'package:album/frameworks/app_theme/index.dart';
import 'package:album/inherited_widgets/index.dart';
import 'package:flutter/material.dart';

class AlbumAppThemeData extends AppThemeData {
  EditTextTheme editTextTheme;
  MultiLineEditTextTheme multiLineEditTextTheme;
  TabBarThemeData tabBarTheme;

  AlbumAppThemeData() {
    editTextTheme = AppEditTextTheme();
    multiLineEditTextTheme = AppMultiLineEditTextTheme();
    backgroundColor = AppColors.appBgColor;
  }

  @override
  ThemeData toTheme() {
    return ThemeData(backgroundColor: backgroundColor);
  }
}

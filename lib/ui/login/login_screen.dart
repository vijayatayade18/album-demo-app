import 'package:album/blocs/index.dart';
import 'package:album/constants/index.dart';
import 'package:album/ui/home/home_screen.dart';
import 'package:album/ui/widgets/buttons/app_buttons.dart';
import 'package:album/ui/widgets/edit_text/index.dart';
import 'package:album/utils/index.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class LoginScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
  LoginBloc _loginBloc = LoginBloc();

  @override
  Widget build(BuildContext context) {
    return Container(
      color: AppColors.appBgColor,
      padding: EdgeInsets.only(
        top: 40,
        left: 20,
        right: 20,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.start,
        mainAxisSize: MainAxisSize.min,
        children: UIUtil.getSeparatedWidgets(
          [
            Text(
              AppStrings.logIn,
              style: TextStyles.logInLabelStyle,
            ),
            AppCustomTextField(
              label: AppStrings.labelEmailId,
              placeHolder: AppStrings.placeholderEmailID,
              controller: _loginBloc.usernameController,
            ),
            AppCustomTextField(
              label: AppStrings.labelPassword,
              placeHolder: AppStrings.placeholderPassword,
              controller: _loginBloc.passwordController,
            ),
            AppButton(
              title: AppStrings.btnContinue,
              onPressed: () {
                _loginBloc.verifyUser(onStart: () {
                  DialogUtil.showLoader(
                    context,
                    AppStrings.loaderTextVerifyingUser,
                  );
                }, onSuccess: () {
                  DialogUtil.hideLoader(context);
                  ScreenNavigator.navigateToHomeScreen(
                    context: context,
                    screen: HomeScreen(),
                  );
                }, onFailure: (String errorMessage) {
                  DialogUtil.hideLoader(context);
                  DialogUtil.showErrorDialog(
                    context,
                    errorMessage,
                  );
                });
              },
            ),
          ],
          interItemSpace: 24,
          flowHorizontal: false,
        ),
      ),
    );
  }
}

import 'package:album/constants/app_colors.dart';
import 'package:album/utils/index.dart';
import 'package:flutter/material.dart';

import 'custom_tab_bar/custom_tab_bar.dart';
import 'tab_index_controller.dart';

class TabBarController extends StatefulWidget {
  final List<String> icons;
  final List<String> deselectedIcons;
  final List<String> texts;
  final Widget overlayWidget;
  final TabIndexController selectedIndex;
  final TabIndexController containerSelectedIndex;
  final Function(int) onTabClick;
  final List<Widget> children;

  TabBarController({
    Key key,
    this.icons = const [],
    this.deselectedIcons = const [],
    this.texts = const [],
    this.selectedIndex,
    this.containerSelectedIndex,
    this.overlayWidget,
    this.onTabClick,
    this.children,
  }) : super(key: key);

  @override
  _TabBarControllerState createState() => _TabBarControllerState();
}

class _TabBarControllerState extends State<TabBarController> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      color: AppColors.appBgColor,
      child: Stack(
        children: <Widget>[
          Column(
            children: <Widget>[
              Expanded(
                child: Stack(
                  children: UIUtil.getSeparatedWidgets(
                    [
                      Container(
                        height: double.infinity,
                        width: double.infinity,
                        child: ValueListenableBuilder(
                          valueListenable: widget.containerSelectedIndex.indexListener,
                          builder: (context, value, __) {
                            return widget.children[value];
                          },
                        ),
                      ),
                      widget.overlayWidget
                    ],
                  ),
                ),
              ),
              ValueListenableBuilder(
                valueListenable: widget.selectedIndex.indexListener,
                builder: (context, value, _) {
                  return CustomTabBar(
                    key: ValueKey("MainTabBar"),
                    selectedIndex: value,
                    icons: widget.icons,
                    deselectedIcons: widget.deselectedIcons,
                    texts: widget.texts,
                    onTabClick: widget.onTabClick,
                  );
                },
              ),
            ],
          ),
        ],
      ),
    );
  }
}

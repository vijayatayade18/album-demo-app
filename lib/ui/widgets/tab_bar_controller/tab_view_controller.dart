typedef OnViewWillAppear = void Function(int index);
typedef OnViewWillDisappear = void Function(int index);
typedef OnShowCoachmark = void Function();

class TabViewController {
  OnViewWillAppear onViewWillAppear;
  OnViewWillDisappear onViewWillDisappear;
}

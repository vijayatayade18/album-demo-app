import 'package:album/constants/app_colors.dart';
import 'package:album/constants/index.dart';
import 'package:album/frameworks/app_theme/index.dart';
import 'package:album/inherited_widgets/theme_provider.dart';
import 'package:album/utils/index.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class CustomTabBar extends StatefulWidget {
  final List<String> icons;
  final List<String> deselectedIcons;
  final List<String> texts;
  final int selectedIndex;
  final Function(int) onTabClick;

  CustomTabBar({
    Key key,
    this.icons = const [],
    this.deselectedIcons = const [],
    this.texts = const [],
    this.selectedIndex = 0,
    this.onTabClick,
  })  : assert(icons.length == texts.length),
        assert(icons.isNotEmpty),
        super(key: key);

  @override
  _CustomTabBarState createState() => _CustomTabBarState();
}

class _CustomTabBarState extends State<CustomTabBar> {
  TabBarThemeData theme;

  @override
  Widget build(BuildContext context) {
    theme = ThemeProvider.of(context).tabBarTheme;

    List<Widget> tabBarItems = [];
    for (int i = 0; i < widget.icons.length; i++) {
      tabBarItems.add(Expanded(
          flex: 1,
          child: FlatButton(
              padding: EdgeInsets.zero,
              splashColor: Colors.transparent,
              highlightColor: Colors.transparent,
              onPressed: () {
                if (widget.onTabClick != null) {
                  widget.onTabClick(i);
                }
              },
              child: Container(
                child: _tabBarWidget(i, (i == widget.selectedIndex)),
              ))));
    }

    return Container(
      decoration: BoxDecoration(
        color: AppColors.tabBarBgColor,
        boxShadow: [
          BoxShadow(
            color: Color.fromRGBO(69, 66, 75, 0.2),
            blurRadius: 12,
            offset: Offset(0, -1),
          ),
        ],
      ),
      child: SafeArea(
        top: false,
        child: Container(
          width: double.infinity,
          padding: EdgeInsets.symmetric(
            horizontal: 12.0,
          ),
          child: Row(
            children: UIUtil.getSeparatedWidgets(
              tabBarItems,
              interItemSpace: 12.0,
            ),
          ),
        ),
      ),
    );
  }

  Container _tabBarWidget(int index, bool isSelected) {
    return Container(
      padding: EdgeInsets.only(left: 8, right: 8, bottom: 4, top: 16),
      child: Column(
        mainAxisSize: MainAxisSize.min,
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          SvgPicture.asset(
            (isSelected) ? widget.icons[index] : widget.deselectedIcons[index],
            height: 22,
            width: 30,
          ),
          SizedBox(
            height: 4,
          ),
          Text(
            widget.texts[index] ?? "",
            textAlign: TextAlign.center,
            overflow: TextOverflow.ellipsis,
            style: (isSelected) ? TextStyles.tabBarTextSelected : TextStyles.tabBarTextUnSelected,
          ),
        ],
      ),
    );
  }
}

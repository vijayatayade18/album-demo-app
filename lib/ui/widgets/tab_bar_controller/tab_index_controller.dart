import 'package:flutter/foundation.dart';

class TabIndexController {
  ValueNotifier<int> _index;

  ValueNotifier<int> get indexListener => _index;

  TabIndexController(int index) {
    _index = ValueNotifier(index);
  }

  int get index => _index.value;

  set index(int value) {
    _index.value = value;
  }
}

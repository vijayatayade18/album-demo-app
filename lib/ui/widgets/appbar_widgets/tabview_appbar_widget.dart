import 'package:album/constants/index.dart';
import 'package:album/utils/index.dart';
import 'package:flutter/cupertino.dart';

class TabViewAppBarWidget extends StatelessWidget {
  final String title;
  final String subTitle;

  const TabViewAppBarWidget({
    Key key,
    this.title,
    this.subTitle,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return _getAppbarWidget();
  }

  Widget _getAppbarWidget() {
    return Container(
      decoration: BoxDecoration(
        color: AppColors.appbarBgColor,
        boxShadow: <BoxShadow>[
          BoxShadow(
            color: Color.fromRGBO(0, 0, 0, 0.16),
            offset: Offset(0.0, 3.0),
            blurRadius: 6,
            spreadRadius: 0.0,
          )
        ],
      ),
      padding: EdgeInsets.only(
        left: 18,
        right: 18,
        top: 20,
      ),
      height: 130,
      width: double.infinity,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        mainAxisAlignment: MainAxisAlignment.start,
        children: UIUtil.getSeparatedWidgets(
          [
            Text(
              title,
              style: TextStyles.appBarTitle.copyWith(
                fontSize: 22,
              ),
            ),
            Text(
              subTitle,
              style: TextStyles.appBarSubTitle,
              maxLines: 2,
              overflow: TextOverflow.ellipsis,
            ),
          ],
          interItemSpace: 10,
          flowHorizontal: false,
        ),
      ),
    );
  }
}

import 'package:album/constants/index.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class AppButton extends StatelessWidget {
  final Decoration decoration;
  final Shadow shadow;
  final String title;
  final bool isPrimary;
  final Color backgroundColor;
  final double borderRadius;
  final VoidCallback onPressed;
  final double width;

  const AppButton({
    Key key,
    this.decoration,
    this.shadow,
    @required this.title,
    this.isPrimary = true,
    this.backgroundColor,
    this.borderRadius = 30.0,
    @required this.onPressed,
    this.width,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      width: width ?? double.infinity,
      decoration: decoration ??
          BoxDecoration(
            color: AppColors.greenColor,
            borderRadius: BorderRadius.circular(
              borderRadius,
            ),
            boxShadow: shadow ??
                <BoxShadow>[
                  BoxShadow(
                    color: Color.fromRGBO(0, 0, 0, 0.16),
                    offset: Offset(0.0, 3.0),
                    blurRadius: 6,
                    spreadRadius: 0.0,
                  )
                ],
          ),
      height: 44,
      child: ClipRRect(
        borderRadius: BorderRadius.circular(
          borderRadius,
        ),
        child: MaterialButton(
          splashColor: AppColors.appBgColor,
          highlightColor: Colors.transparent,
          onPressed: onPressed,
          padding: EdgeInsets.zero,
          minWidth: 0,
          child: Container(
            alignment: Alignment.center,
            padding: EdgeInsets.all(8),
            child: Text(
              title,
              style: TextStyles.primaryButtonStyle,
            ),
          ),
        ),
      ),
    );
  }
}

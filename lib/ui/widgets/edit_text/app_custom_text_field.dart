import 'package:album/constants/index.dart';
import 'package:album/frameworks/app_theme/index.dart';
import 'package:album/frameworks/controller/index.dart';
import 'package:album/frameworks/widgets/custom_input_filed/index.dart';
import 'package:album/inherited_widgets/theme_provider.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class AppCustomTextField extends StatefulWidget {
  final String label;
  final String placeHolder;
  final bool required;
  final int numberOfLines;
  final EditTextController controller;
  final TextInputType textInputType;
  final int maxLength;
  final List<TextInputFormatter> inputFormatters;
  final bool isClearError;
  final bool allowMic;
  final bool autoFocus;
  final bool autoCorrect;
  final VoidCallback onFieldSubmitted;
  final TextInputAction textInputAction;
  final ValueChanged<String> onTextChanged;
  final bool isSecureText;

  const AppCustomTextField({
    Key key,
    this.label,
    this.placeHolder,
    this.required = false,
    this.numberOfLines = 1,
    this.controller,
    this.textInputType,
    this.maxLength,
    this.isSecureText = false,
    this.inputFormatters,
    this.isClearError = true,
    this.allowMic = true,
    this.autoFocus = false,
    this.autoCorrect = true,
    this.onFieldSubmitted,
    this.textInputAction,
    this.onTextChanged,
  }) : super(key: key);

  @override
  _AppCustomTextFieldState createState() => _AppCustomTextFieldState();
}

class _AppCustomTextFieldState
    extends State<AppCustomTextField> {

  bool available = false;
  ValueNotifier<bool> isListening = ValueNotifier(false);

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    AppThemeData appThemeData = ThemeProvider.of(context);

    return AnimatedBuilder(
      animation: widget.controller,
      builder: (context, child) {
        return CustomTextField(
          label: widget.label,
          placeHolder: widget.placeHolder,
          errorMessage: widget.controller.error,
          textController: widget.controller,
          numberOfLines: widget.numberOfLines,
          textInputType: widget.textInputType,
          autoCorrect: widget.autoCorrect,
          maxLength: widget.maxLength,
          secureText: widget.isSecureText,
          onTextChanged: (text) {
            if (widget.isClearError) {
              widget.controller.error = null;
            }
            isListening.value = false;
            if (widget.onTextChanged != null) {
              widget.onTextChanged(text);
            }
            widget.controller.onTextChanged(text);
          },
          onEditingComplete: widget.onFieldSubmitted,
          inputFormatters: widget.controller.inputFormatters,
          errorView: (widget.controller.error != null) ? _errorView() : null,
          textFieldBottomSpace: 4.0,
          enabled: widget.controller.enabled,
          required: widget.required,
          autoFocus: widget.autoFocus,
          cursorColor: appThemeData.editTextTheme.cursorColor,
          textInputAction: widget.textInputAction,
        );
      },
    );
  }


  Row _errorView() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Icon(
          Icons.error_outline,
          color: AppColors.red,
          size: 14,
        ),
        SizedBox(
          width: 5.0,
        ),
        Expanded(
            child: Text(
          widget.controller.error,
          maxLines: 2,
          overflow: TextOverflow.ellipsis,
          style: TextStyles.errorTextStyle,
        ))
      ],
    );
  }

}

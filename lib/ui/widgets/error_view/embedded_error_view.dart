import 'package:album/constants/index.dart';
import 'package:album/ui/widgets/buttons/index.dart';
import 'package:album/utils/index.dart';
import 'package:flutter/cupertino.dart';

class EmbeddedErrorView extends StatelessWidget {
  final String errorMessage;
  final String icon;
  final bool showRetryButton;
  final VoidCallback onRetryClicked;

  const EmbeddedErrorView({
    Key key,
    this.errorMessage,
    this.icon,
    this.showRetryButton,
    this.onRetryClicked,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Container(
      alignment: Alignment.center,
      color: AppColors.appBgColor,
      padding: EdgeInsets.symmetric(
        vertical: 8,
        horizontal: 12,
      ),
      child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: UIUtil.getSeparatedWidgets(
            [
              Image.asset(
                icon ?? Assets.error,
                height: 70,
                width: 70,
                color: AppColors.white,
              ),
              Text(
                errorMessage ?? "Something went wrong, please try again later.",
                textAlign: TextAlign.center,
                style: TextStyles.onScreenErrorTextStyle,
                maxLines: 3,
              ),
              showRetryButton
                  ? AppButton(
                      width: 120,
                      title: AppStrings.btnRetry,
                      onPressed: () {
                        if (onRetryClicked != null) {
                          onRetryClicked();
                        }
                      },
                    )
                  : SizedBox.shrink(),
            ],
            interItemSpace: 16,
            flowHorizontal: false,
          )),
    );
  }
}

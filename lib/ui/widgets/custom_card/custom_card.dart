import 'package:flutter/material.dart';

class CustomCard extends StatelessWidget {
  final EdgeInsetsGeometry padding;
  final EdgeInsetsGeometry margin;
  final double radius;
  final double borderWidth;
  final Color borderColor;
  final Color backgroundColor;
  final List<BoxShadow> shadow;
  final Widget child;
  final Gradient gradient;

  const CustomCard({
    Key key,
    this.padding = EdgeInsets.zero,
    this.margin = EdgeInsets.zero,
    this.radius,
    this.borderWidth,
    this.borderColor,
    this.shadow = const <BoxShadow>[
      BoxShadow(
        color: Color.fromRGBO(25, 25, 25, 0.10),
        offset: Offset(0.0, 3.0),
        blurRadius: 2,
        spreadRadius: 0.0,
      )
    ],
    this.child,
    this.gradient,
    this.backgroundColor = Colors.white,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      type: MaterialType.transparency,
      child: Container(
        margin: margin,
        decoration: BoxDecoration(
          borderRadius: BorderRadius.circular(radius ?? 8.0),
          border: (borderColor != null)
              ? Border.all(
                  color: borderColor ?? Color.fromARGB(0, 192, 192, 192),
                  width: borderWidth ?? 1,
                )
              : null,
          boxShadow: shadow ??
              [
                BoxShadow(
                  offset: Offset(0.0, 4.0),
                  blurRadius: 10.0,
                  spreadRadius: 0.0,
                )
              ],
        ),
        child: ClipRRect(
          borderRadius: BorderRadius.circular(radius ?? 8),
          child: Container(
            padding: padding ?? EdgeInsets.zero,
            alignment: (padding == null) ? Alignment.topLeft : null,
            decoration: BoxDecoration(
              color: backgroundColor ?? Colors.white,
              gradient: gradient,
            ),
            child: child,
          ),
        ),
      ),
    );
  }
}

import 'package:album/blocs/index.dart';
import 'package:album/constants/index.dart';
import 'package:album/data/models/user/index.dart';
import 'package:album/utils/index.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

class ProfileScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _ProfileScreenState();
}

class _ProfileScreenState extends State<ProfileScreen> {
  UserDetailsDTO userDetailsDTO;
  LoginBloc _loginBloc = LoginBloc();

  @override
  void initState() {
    super.initState();
    userDetailsDTO = _loginBloc.getUserDetails();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          _getTopWidget(),
          Expanded(
            child: Container(
              padding: EdgeInsets.symmetric(
                horizontal: 18,
                vertical: 18,
              ),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                children: UIUtil.getSeparatedWidgets(
                  [
                    Text(
                      AppStrings.labelBasicInformation,
                      style: TextStyles.profileInformationHeader,
                    ),
                    _getRow(AppStrings.labelName, userDetailsDTO.name),
                    _getRow(AppStrings.labelEmailAddress, userDetailsDTO.email),
                    _getRow(AppStrings.labelUserName, userDetailsDTO.username),
                    _getRow(AppStrings.labelPhoneNo, userDetailsDTO.phone),
                    _getRow(AppStrings.labelWebsite, userDetailsDTO.website),
                    _getRow(AppStrings.labelCompany, userDetailsDTO.company?.name),
                    Text(
                      AppStrings.labelAddressInformation,
                      style: TextStyles.profileInformationHeader,
                    ),
                    _getRow(AppStrings.labelStreet, userDetailsDTO.address?.street),
                    _getRow(AppStrings.labelSuit, userDetailsDTO.address?.suite),
                    _getRow(AppStrings.labelCity, userDetailsDTO.address?.city),
                    _getRow(AppStrings.labelZipcode, userDetailsDTO.address?.zipcode),
                    GestureDetector(
                      onTap: () async {
                        await _loginBloc.logoutUser();
                      },
                      child: Text(
                        AppStrings.labelLogout,
                        style: TextStyles.profileInformationHeader,
                      ),
                    ),
                  ],
                  interItemSpace: 18,
                  flowHorizontal: false,
                ),
              ),
            ),
          )
        ],
      ),
    );
  }

  Widget _getTopWidget() {
    return Container(
      decoration: BoxDecoration(
        color: AppColors.appbarBgColor,
        boxShadow: <BoxShadow>[
          BoxShadow(
            color: Color.fromRGBO(0, 0, 0, 0.16),
            offset: Offset(0.0, 3.0),
            blurRadius: 6,
            spreadRadius: 0.0,
          )
        ],
      ),
      padding: EdgeInsets.only(left: 18, right: 18, top: 20, bottom: 20),
      height: 100,
      width: double.infinity,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: UIUtil.getSeparatedWidgets(
          [
            Row(
              mainAxisAlignment: MainAxisAlignment.start,
              children: UIUtil.getSeparatedWidgets(
                [
                  Image.asset(
                    Assets.userProfile,
                    height: 50,
                    width: 50,
                  ),
                  Text(
                    AppStrings.labelUserProfile,
                    style: TextStyles.appBarTitle.copyWith(fontSize: 22),
                  ),
                ],
                interItemSpace: 18,
              ),
            ),
          ],
          interItemSpace: 16,
          flowHorizontal: false,
        ),
      ),
    );
  }

  Row _getRow(
    String title,
    String value,
  ) {
    return Row(
      mainAxisAlignment: MainAxisAlignment.spaceBetween,
      children: [
        Text(
          title,
          style: TextStyles.profileInformationTitle,
        ),
        Text(
          value,
          style: TextStyles.profileInformationValue,
        ),
      ],
    );
  }
}

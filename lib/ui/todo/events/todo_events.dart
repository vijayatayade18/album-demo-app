import 'package:equatable/equatable.dart';

abstract class TodoEvents extends Equatable {}

class TodoLoadingStateEvent implements TodoEvents {
  @override
  List<Object> get props => [];

  @override
  bool get stringify => false;
}

class TodoListStateEvent implements TodoEvents {
  @override
  List<Object> get props => [];

  @override
  bool get stringify => false;
}

class TodoErrorStateEvent implements TodoEvents {
  @override
  List<Object> get props => [];

  @override
  bool get stringify => false;
}

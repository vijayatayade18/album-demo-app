import 'package:album/constants/index.dart';
import 'package:album/ui/widgets/shimmer/index.dart';
import 'package:album/utils/index.dart';
import 'package:flutter/material.dart';

class TodoCardShimmer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return _getTodoShimmerCard();
  }

  Widget _getTodoShimmerCard() {
    return Shimmer(
      child: Container(
        decoration: BoxDecoration(
          color: Color(0xff191919),
          borderRadius: BorderRadius.circular(
            4.0,
          ),
          boxShadow: <BoxShadow>[
            BoxShadow(
              color: Color.fromRGBO(0, 0, 0, 0.16),
              offset: Offset(0.0, 3.0),
              blurRadius: 6,
              spreadRadius: 0.0,
            )
          ],
        ),
        padding: EdgeInsets.symmetric(
          horizontal: 18,
          vertical: 18,
        ),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: UIUtil.getSeparatedWidgets(
            [
              Shimmer(
                child: Container(
                  width: 200,
                  height: 10,
                  decoration: BoxDecoration(
                    color: AppColors.shimmerColor,
                    shape: BoxShape.rectangle,
                    borderRadius: BorderRadius.circular(
                      6,
                    ),
                  ),
                ),
              ),
              Shimmer(
                child: Container(
                  width: 100,
                  height: 10,
                  decoration: BoxDecoration(
                    color: AppColors.shimmerColor,
                    shape: BoxShape.rectangle,
                    borderRadius: BorderRadius.circular(
                      6,
                    ),
                  ),
                ),
              ),
            ],
            interItemSpace: 10,
            flowHorizontal: false,
          ),
        ),
      ),
    );
  }
}

import 'package:album/ui/todo/states/index.dart';
import 'package:album/ui/todo/widgets/index.dart';
import 'package:album/utils/index.dart';
import 'package:flutter/material.dart';

class TodoLoadingState extends StatefulWidget implements TodoState {
  @override
  State<StatefulWidget> createState() => _TodoLoadingState();

  @override
  List<Object> get props => [];

  @override
  bool get stringify => false;
}

class _TodoLoadingState extends State<TodoLoadingState> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(
        vertical: 18,
        horizontal: 18,
      ),
      child: Column(
          children: UIUtil.getSeparatedWidgets(
        [
          TodoCardShimmer(),
          TodoCardShimmer(),
        ],
        interItemSpace: 20,
        flowHorizontal: false,
      )),
    );
  }
}

import 'package:album/constants/index.dart';
import 'package:album/data/models/todo/index.dart';
import 'package:album/ui/todo/states/index.dart';
import 'package:album/utils/index.dart';
import 'package:flutter/material.dart';

class TodoListState extends StatefulWidget implements TodoState {
  final List<TodoDetailsDTO> todoList;

  TodoListState(this.todoList);

  @override
  State<StatefulWidget> createState() => _TodoListState();

  @override
  List<Object> get props => [];

  @override
  bool get stringify => false;
}

class _TodoListState extends State<TodoListState> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: ListView.separated(
        shrinkWrap: true,
        padding: EdgeInsets.symmetric(
          vertical: 18,
          horizontal: 18,
        ),
        itemCount: widget.todoList.isNotEmpty ? widget.todoList.length : 0,
        separatorBuilder: (BuildContext context, int index) {
          return SizedBox(
            height: 20,
          );
        },
        itemBuilder: (BuildContext context, int index) {
          return getTodoCard(widget.todoList[index]);
        },
      ),
    );
  }

  Widget getTodoCard(TodoDetailsDTO todoDetailsDTO) {
    return Container(
      decoration: BoxDecoration(
        color: AppColors.listCardBgColor,
        borderRadius: BorderRadius.circular(
          4.0,
        ),
        boxShadow: <BoxShadow>[
          BoxShadow(
            color: Color.fromRGBO(0, 0, 0, 0.16),
            offset: Offset(0.0, 3.0),
            blurRadius: 6,
            spreadRadius: 0.0,
          )
        ],
      ),
      padding: EdgeInsets.symmetric(
        horizontal: 18,
        vertical: 14,
      ),
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: UIUtil.getSeparatedWidgets(
          [
            Text(
              todoDetailsDTO.title,
              style: TextStyles.albumCardText,
            ),
            Text(
              todoDetailsDTO.completed ? AppStrings.completed : AppStrings.inCompleted,
              style: TextStyles.todoStatus(todoDetailsDTO.completed),
            ),
          ],
          interItemSpace: 4,
          flowHorizontal: false,
        ),
      ),
    );
  }
}

import 'package:album/ui/todo/states/index.dart';
import 'package:album/ui/widgets/error_view/index.dart';
import 'package:flutter/material.dart';

class TodoErrorState extends StatefulWidget implements TodoState {
  final String errorMessage;
  final String icon;
  final bool showRetryButton;
  final VoidCallback onRetryClicked;

  const TodoErrorState({
    Key key,
    this.errorMessage,
    this.icon,
    this.showRetryButton = true,
    this.onRetryClicked,
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() => _TodoErrorState();

  @override
  List<Object> get props => [];

  @override
  bool get stringify => false;
}

class _TodoErrorState extends State<TodoErrorState> {
  @override
  Widget build(BuildContext context) {
    return EmbeddedErrorView(
      icon: widget.icon,
      onRetryClicked: widget.onRetryClicked,
      showRetryButton: widget.showRetryButton,
      errorMessage: widget.errorMessage,
    );
  }
}

import 'package:album/blocs/todo_bloc.dart';
import 'package:album/constants/index.dart';
import 'package:album/ui/todo/states/index.dart';
import 'package:album/ui/widgets/appbar_widgets/index.dart';
import 'package:album/utils/index.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class TodoListScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _TodoListScreenState();
}

class _TodoListScreenState extends State<TodoListScreen> {
  @override
  Widget build(BuildContext context) {
    return Container(
        child: Column(crossAxisAlignment: CrossAxisAlignment.start, children: [
      TabViewAppBarWidget(
        title: AppStrings.labelTodoList,
        subTitle: "ashdouasodam jhdousah a sdosad sajduasd shuhsd  sjhsudsan , oushdjs  ",
      ),
      Expanded(
        child: BlocProvider(
          create: (BuildContext context) => TodoBloc(
            TodoLoadingState(),
          ),
          child: Builder(builder: (context) {
            return BlocBuilder<TodoBloc, TodoState>(
              builder: (context, state) => state as Widget,
            );
          }),
        ),
      ),
    ]));
  }

  Widget _getTopWidget() {
    return Container(
      color: Color(0xff191919),
      padding: EdgeInsets.only(
        left: 18,
        right: 18,
        top: 20,
        bottom: 20,
      ),
      width: double.infinity,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: UIUtil.getSeparatedWidgets(
          [
            Text(
              AppStrings.labelTodoList,
              style: TextStyles.appBarTitle,
            ),
            Text(
              "sjhdjshdljsah",
              style: TextStyles.appBarTitle,
            ),
          ],
          interItemSpace: 16,
          flowHorizontal: false,
        ),
      ),
    );
  }
}

import 'package:album/blocs/index.dart';
import 'package:album/constants/index.dart';
import 'package:album/data/models/albums/index.dart';
import 'package:album/ui/album/states/index.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';

import 'events/album_events.dart';

class AlbumDetailsScreen extends StatefulWidget {
  final AlbumDetailsDTO albumDetailsDTO;

  const AlbumDetailsScreen({
    Key key,
    this.albumDetailsDTO,
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() => _AlbumDetailsScreenState();
}

class _AlbumDetailsScreenState extends State<AlbumDetailsScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.appBgColor,
      appBar: _getAppbar(context),
      body: BlocProvider(
        create: (BuildContext context) => AlbumBloc(
          AlbumPhotosLoadingState(),
          AlbumPhotosLoadingStateEvent(),
          albumId: widget.albumDetailsDTO.id,
        ),
        child: Builder(builder: (context) {
          return BlocBuilder<AlbumBloc, AlbumState>(
            builder: (context, state) => state as Widget,
          );
        }),
      ),
    );
  }

  AppBar _getAppbar(BuildContext context) {
    return AppBar(
      title: Text(
        widget.albumDetailsDTO.title ?? "",
        style: TextStyles.appBarTitle,
        textAlign: TextAlign.start,
      ),
      leadingWidth: 20,
      leading: GestureDetector(
        onTap: () {
          Navigator.of(context).pop();
        },
        child: Container(
          margin: EdgeInsets.only(left: 10),
          child: SvgPicture.asset(
            Assets.backArrow,
          ),
        ),
      ),
      backgroundColor: AppColors.appbarBgColor,
    );
  }

}

import 'package:equatable/equatable.dart';

abstract class AlbumEvents extends Equatable {}

class AlbumLoadingStateEvent implements AlbumEvents {
  @override
  List<Object> get props => [];

  @override
  bool get stringify => false;
}

class AlbumListStateEvent implements AlbumEvents {
  @override
  List<Object> get props => [];

  @override
  bool get stringify => false;
}

class AlbumErrorStateEvent implements AlbumEvents {
  @override
  List<Object> get props => [];

  @override
  bool get stringify => false;
}

class AlbumPhotosLoadingStateEvent implements AlbumEvents {
  @override
  List<Object> get props => [];

  @override
  bool get stringify => false;
}

import 'package:album/blocs/index.dart';
import 'package:album/constants/index.dart';
import 'package:album/ui/album/events/album_events.dart';
import 'package:album/ui/album/states/index.dart';
import 'package:album/ui/widgets/appbar_widgets/index.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class AlbumScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _AlbumScreenState();
}

class _AlbumScreenState extends State<AlbumScreen> {
  @override
  Widget build(BuildContext context) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          TabViewAppBarWidget(
            title: AppStrings.labelAlbum,
            subTitle: "sjds  saidhs isaahdns d hsidsa d sjdhn suds jhshd sdigsdb s dsid  hsgdhs sadous  dusd s dusd shsaads svds as disgds d ",
          ),
          Expanded(
            child: BlocProvider(
              create: (BuildContext context) => AlbumBloc(
                AlbumLoadingState(),
                AlbumLoadingStateEvent(),
              ),
              child: Builder(builder: (context) {
                return BlocBuilder<AlbumBloc, AlbumState>(
                  builder: (context, state) => state as Widget,
                );
              }),
            ),
          ),
        ],
      ),
    );
  }
}

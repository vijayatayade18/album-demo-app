import 'package:album/data/models/albums/index.dart';
import 'package:album/ui/album/states/index.dart';
import 'package:album/ui/album/widgets/album_card.dart';
import 'package:album/ui/widgets/network_image_view/index.dart';
import 'package:flutter/material.dart';

class AlbumPhotoListState extends StatefulWidget implements AlbumState {
  final List<AlbumPhotoDetails> albumPhotoList;

  AlbumPhotoListState(this.albumPhotoList);

  @override
  State<StatefulWidget> createState() => _AlbumPhotoListState();

  @override
  List<Object> get props => [];

  @override
  bool get stringify => false;
}

class _AlbumPhotoListState extends State<AlbumPhotoListState> {
  LoadedImageBuilder widgetBuilder;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: ListView.separated(
        shrinkWrap: true,
        padding: EdgeInsets.symmetric(
          vertical: 18,
          horizontal: 18,
        ),
        itemCount: widget.albumPhotoList.isNotEmpty ? widget.albumPhotoList.length : 0,
        separatorBuilder: (BuildContext context, int index) {
          return SizedBox(
            height: 20,
          );
        },
        itemBuilder: (BuildContext context, int index) {
          return AlbumCard(
            albumPhotoDetails: widget.albumPhotoList[index],
          );
        },
      ),
    );
  }
}

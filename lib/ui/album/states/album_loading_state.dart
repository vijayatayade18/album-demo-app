import 'package:album/ui/album/states/index.dart';
import 'package:album/ui/album/widgets/index.dart';
import 'package:album/utils/index.dart';
import 'package:flutter/material.dart';

class AlbumLoadingState extends StatefulWidget implements AlbumState {
  @override
  State<StatefulWidget> createState() => _AlbumLoadingState();

  @override
  List<Object> get props => [];

  @override
  bool get stringify => false;
}

class _AlbumLoadingState extends State<AlbumLoadingState> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(
        vertical: 18,
        horizontal: 18,
      ),
      child: Column(
          children: UIUtil.getSeparatedWidgets(
        [
          AlbumCardShimmer(),
          AlbumCardShimmer(),
          AlbumCardShimmer(),
          AlbumCardShimmer(),
        ],
        interItemSpace: 20,
        flowHorizontal: false,
      )),
    );
  }
}

import 'package:album/constants/index.dart';
import 'package:album/data/models/albums/index.dart';
import 'package:album/ui/album/album_details_screen.dart';
import 'package:album/ui/album/states/index.dart';
import 'package:album/ui/widgets/network_image_view/index.dart';
import 'package:album/utils/index.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class AlbumListState extends StatefulWidget implements AlbumState {
  final List<AlbumDetailsDTO> albumList;

  AlbumListState(this.albumList);

  @override
  State<StatefulWidget> createState() => _AlbumListState();

  @override
  List<Object> get props => [];

  @override
  bool get stringify => false;
}

class _AlbumListState extends State<AlbumListState> {
  LoadedImageBuilder widgetBuilder;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: ListView.separated(
        shrinkWrap: true,
        padding: EdgeInsets.symmetric(
          vertical: 18,
          horizontal: 18,
        ),
        itemCount: widget.albumList.isNotEmpty ? widget.albumList.length : 0,
        separatorBuilder: (BuildContext context, int index) {
          return SizedBox(
            height: 20,
          );
        },
        itemBuilder: (BuildContext context, int index) {
          return getAlbumCard(widget.albumList[index]);
        },
      ),
    );
  }

  Widget getAlbumCard(AlbumDetailsDTO albumDetails) {
    return GestureDetector(
      onTap: () {
        _navigateToAlbumDetailsScreen(albumDetails);
      },
      child: Container(
        decoration: BoxDecoration(
          color: AppColors.listCardBgColor,
          borderRadius: BorderRadius.circular(
            4.0,
          ),
          boxShadow: <BoxShadow>[
            BoxShadow(
              color: Color.fromRGBO(0, 0, 0, 0.16),
              offset: Offset(0.0, 3.0),
              blurRadius: 6,
              spreadRadius: 0.0,
            )
          ],
        ),
        padding: EdgeInsets.symmetric(horizontal: 18, vertical: 20),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: UIUtil.getSeparatedWidgets(
            [
              Expanded(
                child: Text(
                  albumDetails.title,
                  style: TextStyles.albumCardText,
                  maxLines: 1,
                  overflow: TextOverflow.ellipsis,
                ),
              ),
              SvgPicture.asset(
                Assets.farwordArrow,
                height: 20,
                width: 20,
              )
            ],
            interItemSpace: 18,
          ),
        ),
      ),
    );
  }

  Future _navigateToAlbumDetailsScreen(AlbumDetailsDTO albumDetails) {
    return Navigator.push(
      context,
      PageRouteBuilder(
        pageBuilder: (context, animation, secondaryAnimation) => AlbumDetailsScreen(
          albumDetailsDTO: albumDetails,
        ),
        transitionDuration: Duration(milliseconds: 500),
        transitionsBuilder: (context, animation, secondaryAnimation, child) {
          var begin = Offset(0.0, 1.0);
          var end = Offset.zero;
          var curve = Curves.ease;
          var tween = Tween(begin: begin, end: end).chain(CurveTween(curve: curve));
          return SlideTransition(
            position: animation.drive(tween),
            child: child,
          );
        },
      ),
    );
  }
}

import 'package:album/ui/album/states/index.dart';
import 'package:album/ui/widgets/error_view/index.dart';
import 'package:flutter/material.dart';

class AlbumErrorState extends StatefulWidget implements AlbumState {
  final String errorMessage;
  final String icon;
  final bool showRetryButton;
  final VoidCallback onRetryClicked;

  const AlbumErrorState({
    Key key,
    this.errorMessage,
    this.icon,
    this.showRetryButton = true,
    this.onRetryClicked,
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() => _AlbumErrorState();

  @override
  List<Object> get props => [];

  @override
  bool get stringify => false;
}

class _AlbumErrorState extends State<AlbumErrorState> {
  @override
  Widget build(BuildContext context) {
    return EmbeddedErrorView(
      icon: widget.icon,
      onRetryClicked: widget.onRetryClicked,
      showRetryButton: widget.showRetryButton,
      errorMessage: widget.errorMessage,
    );
  }
}

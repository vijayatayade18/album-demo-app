import 'package:album/constants/index.dart';
import 'package:album/ui/widgets/custom_card/index.dart';
import 'package:album/ui/widgets/shimmer/index.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class AlbumPhotoCardShimmer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Shimmer(
      child: CustomCard(
        backgroundColor: AppColors.listCardBgColor,
        radius: 10,
        child: Container(
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Container(
                padding: EdgeInsets.all(
                  20,
                ),
                width: double.infinity,
                height: 200,
                child: SvgPicture.asset(
                  Assets.appLogo,
                ),
              ),
            ],
          ),
        ),
      ),
    );
  }
}

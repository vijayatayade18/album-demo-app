import 'dart:io';

import 'package:album/constants/index.dart';
import 'package:album/data/models/albums/album_photo_details_dto.dart';
import 'package:album/ui/widgets/custom_card/index.dart';
import 'package:album/ui/widgets/network_image_view/index.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class AlbumCard extends StatelessWidget {
  final AlbumPhotoDetails albumPhotoDetails;

  final LoadedImageBuilder widgetBuilder;

  const AlbumCard({
    Key key,
    this.widgetBuilder,
    this.albumPhotoDetails,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return CustomCard(
      backgroundColor: AppColors.listCardBgColor,
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: [
            Container(
              width: double.infinity,
              height: 200,
              child: _getNetworkImage(
               albumPhotoDetails.url,
              ),
            ),
            Container(
              padding: EdgeInsets.symmetric(
                horizontal: 12,
                vertical: 12,
              ),
              child: Text(
                "data idias  pudiosahdsa nuahdosa, saiss d sa dsaouduashdsa  o",
                style: TextStyles.profileInformationTitle,
              ),
            ),
          ],
        ),
      ),
    );
  }

  NetworkImageView _getNetworkImage(String url) {
    return NetworkImageView(
      url,
      placeholderWidget: Container(
        padding: EdgeInsets.all(
          10.0,
        ),
        child: Container(
          padding: EdgeInsets.all(
            20,
          ),
          width: double.infinity,
          height: 200,
          child: SvgPicture.asset(
            Assets.appLogo,
          ),
        ),
      ),
      imageBuilder: (file) => _getLoadedImage(file),
    );
  }

  Widget _getLoadedImage(File file) {
    if (widgetBuilder == null) {
      return Image.file(
        file,
        fit: BoxFit.fill,
        height: 120,
        width: 100,
      );
    } else {
      return widgetBuilder(file);
    }
  }
}

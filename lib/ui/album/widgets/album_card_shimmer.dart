import 'package:album/constants/index.dart';
import 'package:album/ui/widgets/shimmer/index.dart';
import 'package:album/utils/index.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class AlbumCardShimmer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return getAlbumShimmerCard();
  }

  Widget getAlbumShimmerCard() {
    return Shimmer(
      child: Container(
        decoration: BoxDecoration(
          color: Color(0xff191919),
          borderRadius: BorderRadius.circular(
            4.0,
          ),
          boxShadow: <BoxShadow>[
            BoxShadow(
              color: Color.fromRGBO(0, 0, 0, 0.16),
              offset: Offset(0.0, 3.0),
              blurRadius: 6,
              spreadRadius: 0.0,
            )
          ],
        ),
        padding: EdgeInsets.symmetric(horizontal: 18, vertical: 20),
        child: Row(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: UIUtil.getSeparatedWidgets(
            [
              Expanded(
                child: Container(
                  width: 140,
                  height: 10,
                  decoration: BoxDecoration(
                    color: AppColors.shimmerColor,
                    shape: BoxShape.rectangle,
                    borderRadius: BorderRadius.circular(
                      6,
                    ),
                  ),
                ),
              ),
              SvgPicture.asset(
                Assets.farwordArrow,
                height: 20,
                width: 20,
              )
            ],
            interItemSpace: 18,
          ),
        ),
      ),
    );
  }
}

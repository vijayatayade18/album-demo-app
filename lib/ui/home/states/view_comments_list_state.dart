import 'package:album/constants/index.dart';
import 'package:album/data/models/comments/comments_details_dto.dart';
import 'package:album/ui/home/states/home_states.dart';
import 'package:album/ui/widgets/network_image_view/index.dart';
import 'package:album/utils/index.dart';
import 'package:flutter/material.dart';

class ViewCommentsListState extends StatefulWidget implements HomeState {
  final List<CommentsDetailsDTO> commentsList;

  ViewCommentsListState(this.commentsList);

  @override
  State<StatefulWidget> createState() => _ViewCommentsListState();

  @override
  List<Object> get props => [];

  @override
  bool get stringify => false;
}

class _ViewCommentsListState extends State<ViewCommentsListState> {
  LoadedImageBuilder widgetBuilder;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: ListView.separated(
        shrinkWrap: true,
        padding: EdgeInsets.symmetric(
          vertical: 18,
          horizontal: 18,
        ),
        itemCount: widget.commentsList.isNotEmpty ? widget.commentsList.length : 0,
        separatorBuilder: (BuildContext context, int index) {
          return SizedBox(
            height: 20,
          );
        },
        itemBuilder: (BuildContext context, int index) {
          return _getPostDetailsView(widget.commentsList[index]);
        },
      ),
    );
  }

  Widget _getPostDetailsView(CommentsDetailsDTO comment) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: UIUtil.getSeparatedWidgets(
          [
            Row(
              mainAxisAlignment: MainAxisAlignment.spaceBetween,
              crossAxisAlignment: CrossAxisAlignment.start,
              children: UIUtil.getSeparatedWidgets(
                [
                  Expanded(
                    child: Text(
                      comment.name,
                      style: TextStyles.postTitle,
                      maxLines: 3,
                      overflow: TextOverflow.ellipsis,
                    ),
                  ),
                  Text(
                    comment.email,
                    style: TextStyles.postDescription,
                    maxLines: 3,
                    overflow: TextOverflow.ellipsis,
                  ),
                ],
                interItemSpace: 12,
              ),
            ),
            Text(
              comment.body,
              style: TextStyles.postDescription,
            ),
          ],
          interItemSpace: 2,
          flowHorizontal: false,
        ),
      ),
    );
  }
}

import 'package:album/ui/home/states/index.dart';
import 'package:album/ui/widgets/error_view/index.dart';
import 'package:flutter/material.dart';

class ViewCommentsErrorState extends StatefulWidget implements HomeState {
  final String errorMessage;
  final String icon;
  final bool showRetryButton;
  final VoidCallback onRetryClicked;

  const ViewCommentsErrorState({
    Key key,
    this.errorMessage,
    this.icon,
    this.showRetryButton = true,
    this.onRetryClicked,
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() => _ViewCommentsErrorState();

  @override
  List<Object> get props => [];

  @override
  bool get stringify => false;
}

class _ViewCommentsErrorState extends State<ViewCommentsErrorState> {
  @override
  Widget build(BuildContext context) {
    return EmbeddedErrorView(
      icon: widget.icon,
      onRetryClicked: widget.onRetryClicked,
      showRetryButton: widget.showRetryButton,
      errorMessage: widget.errorMessage,
    );
  }
}

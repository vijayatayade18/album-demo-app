import 'package:album/ui/home/states/index.dart';
import 'package:album/ui/home/widgets/index.dart';
import 'package:album/utils/index.dart';
import 'package:flutter/material.dart';

class ViewCommentsLoadingState extends StatefulWidget implements HomeState {
  @override
  State<StatefulWidget> createState() => _ViewCommentsLoadingState();

  @override
  List<Object> get props => [];

  @override
  bool get stringify => false;
}

class _ViewCommentsLoadingState extends State<ViewCommentsLoadingState> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.symmetric(
        vertical: 18,
        horizontal: 18,
      ),
      child: Column(
          children: UIUtil.getSeparatedWidgets(
        [
          PostFeedShimmer(),
          PostFeedShimmer(),
          PostFeedShimmer(),
        ],
        interItemSpace: 20,
        flowHorizontal: false,
      )),
    );
  }
}

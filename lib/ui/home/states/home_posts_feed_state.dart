import 'package:album/constants/index.dart';
import 'package:album/data/models/posts/index.dart';
import 'package:album/ui/home/create_post_screen.dart';
import 'package:album/ui/home/states/home_states.dart';
import 'package:album/ui/home/view_comments_screen.dart';
import 'package:album/ui/widgets/network_image_view/index.dart';
import 'package:album/utils/index.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class HomePostsFeedState extends StatefulWidget implements HomeState {
  final List<PostDetailsDTO> postsList;

  HomePostsFeedState(this.postsList);

  @override
  State<StatefulWidget> createState() => _HomePostsFeedState();

  @override
  List<Object> get props => [];

  @override
  bool get stringify => false;
}

class _HomePostsFeedState extends State<HomePostsFeedState> {
  LoadedImageBuilder widgetBuilder;

  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Stack(children: [
      Container(
        child: ListView.separated(
          shrinkWrap: true,
          padding: EdgeInsets.symmetric(
            vertical: 18,
            horizontal: 18,
          ),
          itemCount: widget.postsList.isNotEmpty ? widget.postsList.length : 0,
          separatorBuilder: (BuildContext context, int index) {
            return SizedBox(
              height: 20,
            );
          },
          itemBuilder: (BuildContext context, int index) {
            return _getPostDetailsView(widget.postsList[index]);
          },
        ),
      ),
      Positioned(
        bottom: 20,
        right: 20,
        child: _getFabButton(),
      ),
    ]);
  }

  Widget _getPostDetailsView(PostDetailsDTO postDetails) {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: UIUtil.getSeparatedWidgets(
          [
            Text(
              postDetails.title,
              style: TextStyles.postTitle,
            ),
            Text(
              postDetails.body,
              style: TextStyles.postDescription,
            ),
            GestureDetector(
              onTap: () {
                ScreenNavigator.navigateToHomeScreen(
                  context: context,
                  screen: ViewCommentsScreen(
                    postDetailsDTO: postDetails,
                  ),
                );
              },
              child: Text(
                AppStrings.btnViewComments,
                style: TextStyles.viewCommentLabel,
              ),
            ),
          ],
          interItemSpace: 2,
          flowHorizontal: false,
        ),
      ),
    );
  }

  Widget _getFabButton() {
    return GestureDetector(
      onTap: () {
        _navigateToCreatePostScreen();
      },
      child: Container(
        height: 60,
        width: 60,
        decoration: BoxDecoration(
          shape: BoxShape.circle,
        ),
        child: SvgPicture.asset(
          Assets.fabIcon,
          width: 60,
          height: 60,
          alignment: Alignment.center,
        ),
      ),
    );
  }

  Future _navigateToCreatePostScreen() {
    return Navigator.push(
      context,
      PageRouteBuilder(
        pageBuilder: (context, animation, secondaryAnimation) => CreatePostScreen(),
        transitionDuration: Duration(milliseconds: 500),
        transitionsBuilder: (context, animation, secondaryAnimation, child) {
          var begin = Offset(0.0, 1.0);
          var end = Offset.zero;
          var curve = Curves.ease;
          var tween = Tween(begin: begin, end: end).chain(CurveTween(curve: curve));
          return SlideTransition(
            position: animation.drive(tween),
            child: child,
          );
        },
      ),
    );
  }
}

import 'package:album/blocs/index.dart';
import 'package:album/constants/index.dart';
import 'package:album/data/models/posts/index.dart';
import 'package:album/ui/home/events/index.dart';
import 'package:album/ui/home/states/index.dart';
import 'package:album/ui/home/states/view_comments_loading_state.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';

class ViewCommentsScreen extends StatefulWidget {
  final PostDetailsDTO postDetailsDTO;

  const ViewCommentsScreen({
    Key key,
    this.postDetailsDTO,
  }) : super(key: key);

  @override
  State<StatefulWidget> createState() => _ViewCommentsScreen();
}

class _ViewCommentsScreen extends State<ViewCommentsScreen> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.appBgColor,
      appBar: _getAppbar(context),
      body: BlocProvider(
        create: (BuildContext context) => HomeBloc(
          ViewCommentsLoadingState(),
          ViewCommentsLoadingEvent(),
          postId: widget.postDetailsDTO.id,
        ),
        child: Builder(builder: (context) {
          return BlocBuilder<HomeBloc, HomeState>(
            builder: (context, state) => state as Widget,
          );
        }),
      ),
    );
  }

  AppBar _getAppbar(BuildContext context) {
    return AppBar(
      title: Text(
        AppStrings.titleViewComments,
        style: TextStyles.appBarTitle,
        textAlign: TextAlign.start,
      ),
      leadingWidth: 20,
      leading: GestureDetector(
        onTap: () {
          Navigator.of(context).pop();
        },
        child: Container(
          margin: EdgeInsets.only(left: 10),
          child: SvgPicture.asset(
            Assets.backArrow,
          ),
        ),
      ),
      backgroundColor: AppColors.appbarBgColor,
    );
  }
}

import 'package:equatable/equatable.dart';

abstract class HomeEvents extends Equatable {}

class LoadingStateEvent implements HomeEvents {
  @override
  List<Object> get props => [];

  @override
  bool get stringify => false;
}

class PostsStateEvent implements HomeEvents {
  @override
  List<Object> get props => [];

  @override
  bool get stringify => false;
}

class PostsErrorStateEvent implements HomeEvents {
  @override
  List<Object> get props => [];

  @override
  bool get stringify => false;
}

class ViewCommentsSuccessEvent implements HomeEvents {
  @override
  List<Object> get props => [];

  @override
  bool get stringify => false;
}

class ViewCommentsErrorEvent implements HomeEvents {
  @override
  List<Object> get props => [];

  @override
  bool get stringify => false;
}

class ViewCommentsLoadingEvent implements HomeEvents {
  @override
  List<Object> get props => [];

  @override
  bool get stringify => false;
}

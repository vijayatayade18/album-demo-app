import 'dart:async';

import 'package:album/constants/index.dart';
import 'package:album/main.dart';
import 'package:album/ui/album/album_list_screen.dart';
import 'package:album/ui/home/home_feed_screen.dart';
import 'package:album/ui/profile/profile_screen.dart';
import 'package:album/ui/todo/todo_list_screen.dart';
import 'package:album/ui/widgets/tab_bar_controller/index.dart';
import 'package:flutter/material.dart';

class _TabInfo {
  final String selectedIcon;
  final String deselectedIcon;
  final String title;
  final Widget child;
  final TabViewController controller;

  _TabInfo({
    @required this.selectedIcon,
    @required this.deselectedIcon,
    @required this.title,
    this.child,
    this.controller,
  });
}

class HomeTabView extends StatefulWidget {
  final TabIndexController tabIndexController;
  final VoidCallback onTabChange;

  HomeTabView({
    Key key,
    @required this.tabIndexController,
    this.onTabChange,
  }) : super(key: key);

  @override
  _HomeTabViewState createState() => _HomeTabViewState();
}

class _HomeTabViewState extends State<HomeTabView> {
  _TabInfo _scanTab;
  List<_TabInfo> _tabInfos = [];
  final Map<String, dynamic> stateCache = Map<String, dynamic>();

  @override
  void initState() {
    super.initState();
    _setupTabViews();
  }

  @override
  Widget build(BuildContext context) {
    return WillPopScope(
      onWillPop: _onBackPress,
      child: Stack(
        children: [
          _tabBarView(),
        ],
      ),
    );
  }

  Widget _tabBarView() {
    return SafeArea(
      top: true,
      child: TabBarController(
        icons: _tabInfos.map((e) => e.selectedIcon).toList(growable: false),
        deselectedIcons: _tabInfos.map((e) => e.deselectedIcon).toList(growable: false),
        texts: _tabInfos.map((e) => e.title).toList(growable: false),
        onTabClick: (index) async {
          int indexOfScanTab = _tabInfos.indexOf(_scanTab);
          if (index == indexOfScanTab) {
            // DynamicLinks.handleMatchingPath(AppCTA.SCAN);
          } else {
            _handleTabSwitch(index);
          }
        },
        selectedIndex: widget.tabIndexController,
        containerSelectedIndex: widget.tabIndexController,
        children: _tabInfos.map((e) => e.child).toList(growable: false),
      ),
    );
  }

  void _setupTabViews() {
    _tabInfos = [
      _TabInfo(
        selectedIcon: Assets.tabHomeSelected,
        deselectedIcon: Assets.tabHomeDeSelected,
        title: AppStrings.tabHome,
        child: HomeFeedScreen(),
      ),
      _TabInfo(
        selectedIcon: Assets.tabAlbumSelected,
        deselectedIcon: Assets.tabAlbumDeselected,
        title: AppStrings.tabAlbum,
        child: AlbumScreen(),
      ),
      _TabInfo(
        selectedIcon: Assets.tabToDoListSelected,
        deselectedIcon: Assets.tabToDoListDeselected,
        title: AppStrings.tabToDoList,
        child: TodoListScreen(),
      ),
      _TabInfo(
        selectedIcon: Assets.tabProfileSelected,
        deselectedIcon: Assets.tabProfileDeselected,
        title: AppStrings.tabProfile,
        child: ProfileScreen(),
      )
    ];
  }

  void _handleTabSwitch(int index) {
    int currentTabIndex = widget.tabIndexController.index;
    widget.tabIndexController.index = index;
    if (index != currentTabIndex) {
      if (_tabInfos[index].controller?.onViewWillAppear != null) {
        _tabInfos[index].controller.onViewWillAppear(index);
      }
      if (_tabInfos[currentTabIndex].controller?.onViewWillDisappear != null) {
        _tabInfos[currentTabIndex].controller.onViewWillDisappear(currentTabIndex);
      }
      if (widget.onTabChange != null) {
        widget.onTabChange();
      }
    }
  }

  Future<bool> _onBackPress() async {
    int currentTabIndex = widget.tabIndexController.index;
    int previousTabToSwitch;
    for (int i = (currentTabIndex - 1); i >= 0; i--) {
      if (_tabInfos[i].child != null) {
        previousTabToSwitch = i;
        break;
      }
    }
    if (previousTabToSwitch == null) {
      currentAppState.value = AppState.LOGGED_IN;
      return true;
    } else {
      _handleTabSwitch(previousTabToSwitch);
      return false;
    }
  }
}

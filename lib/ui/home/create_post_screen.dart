import 'package:album/blocs/index.dart';
import 'package:album/constants/index.dart';
import 'package:album/frameworks/widgets/custom_input_filed/index.dart';
import 'package:album/ui/home/events/home_events.dart';
import 'package:album/ui/home/states/index.dart';
import 'package:album/ui/widgets/buttons/index.dart';
import 'package:album/ui/widgets/edit_text/index.dart';
import 'package:album/utils/index.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_svg/flutter_svg.dart';

class CreatePostScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _CreatePostScreen();
}

class _CreatePostScreen extends State<CreatePostScreen> {
  HomeBloc _homeBloc = HomeBloc(HomeLoadingState(), LoadingStateEvent());

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.appBgColor,
      appBar: _getAppbar(context),
      body: SingleChildScrollView(
        child: BlocProvider(
          create: (BuildContext context) => _homeBloc,
          child: Builder(builder: (context) {
            return BlocBuilder<HomeBloc, HomeState>(
              builder: (context, state) {
                return Container(
                  padding: EdgeInsets.symmetric(
                    horizontal: 18,
                    vertical: 20,
                  ),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: UIUtil.getSeparatedWidgets(
                      [
                        AppCustomTextField(
                          label: AppStrings.labelTitle,
                          placeHolder: AppStrings.placeholderTitle,
                          controller: _homeBloc.postTitleController,
                        ),
                        AnimatedBuilder(
                          animation: _homeBloc.postController,
                          builder: (context, child) {
                            return MultiLineEditText(
                              label: AppStrings.labelPost,
                              placeHolder: AppStrings.placeholderPost,
                              textController: _homeBloc.postController,
                              errorView: _errorView(),
                            );
                          },
                        ),
                        AppButton(
                          title: AppStrings.btnSubmitPost,
                          onPressed: () {
                            _homeBloc.createPost(onStart: () {
                              DialogUtil.showLoader(
                                context,
                                AppStrings.createPostLoadingMessage,
                              );
                            }, onSuccess: () {
                              DialogUtil.hideLoader(context);
                              Navigator.of(context).pop();
                            }, onFailure: () {
                              DialogUtil.hideLoader(context);
                              DialogUtil.showErrorDialog(context, AppStrings.errorMessage);
                            });
                          },
                        ),
                      ],
                      interItemSpace: 20,
                      flowHorizontal: false,
                    ),
                  ),
                );
              },
            );
          }),
        ),
      ),
    );
  }

  AppBar _getAppbar(BuildContext context) {
    return AppBar(
      title: Text(
        AppStrings.titleCreatePost,
        style: TextStyles.appBarTitle,
        textAlign: TextAlign.start,
      ),
      leadingWidth: 20,
      leading: GestureDetector(
        onTap: () {
          Navigator.of(context).pop();
        },
        child: Container(
          margin: EdgeInsets.only(left: 10),
          child: SvgPicture.asset(
            Assets.backArrow,
          ),
        ),
      ),
      backgroundColor: AppColors.appbarBgColor,
    );
  }

  Row _errorView() {
    return Row(
      mainAxisAlignment: MainAxisAlignment.start,
      children: <Widget>[
        Icon(
          Icons.error_outline,
          color: AppColors.red,
          size: 14,
        ),
        SizedBox(
          width: 5.0,
        ),
        Expanded(
            child: Text(
          _homeBloc.postController.error ?? "",
          maxLines: 2,
          overflow: TextOverflow.ellipsis,
          style: TextStyles.errorTextStyle,
        ))
      ],
    );
  }
}

import 'package:album/blocs/home_bloc.dart';
import 'package:album/constants/index.dart';
import 'package:album/ui/home/events/index.dart';
import 'package:album/ui/home/states/index.dart';
import 'package:album/ui/widgets/appbar_widgets/tabview_appbar_widget.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class HomeFeedScreen extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _HomeFeedScreen();
}

class _HomeFeedScreen extends State<HomeFeedScreen> {
  @override
  Widget build(BuildContext context) {
    return Container(
      color: AppColors.appBgColor,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: [
          TabViewAppBarWidget(
            title: AppStrings.titlePosts,
            subTitle: "ashdouasodam jhdousah a sdosad sajduasd shuhsd  sjhsudsan , oushdjs  ",
          ),
          Expanded(
            child: BlocProvider(
              create: (BuildContext context) => HomeBloc(HomeLoadingState(), LoadingStateEvent()),
              child: Builder(builder: (context) {
                return BlocBuilder<HomeBloc, HomeState>(
                  builder: (context, state) => state as Widget,
                );
              }),
            ),
          )
        ],
      ),
    );
  }
}

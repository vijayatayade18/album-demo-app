import 'package:album/constants/index.dart';
import 'package:album/ui/widgets/shimmer/index.dart';
import 'package:album/utils/index.dart';
import 'package:flutter/cupertino.dart';

class PostFeedShimmer extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return _getPostDetailsShimmerView();
  }

  Widget _getPostDetailsShimmerView() {
    return Container(
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: UIUtil.getSeparatedWidgets(
          [
            _getContainer(height: 8, width: 200),
            _getContainer(height: 8, width: 160),
            _getContainer(height: 8, width: 100),
          ],
          interItemSpace: 10,
          flowHorizontal: false,
        ),
      ),
    );
  }

  Widget _getContainer({@required double height, @required double width}) {
    return Shimmer(
      child: Container(
        height: height ?? 8,
        width: width ?? 200,
        decoration: BoxDecoration(
          color: AppColors.shimmerColor,
          shape: BoxShape.rectangle,
          borderRadius: BorderRadius.circular(
            6,
          ),
        ),
      ),
    );
  }
}

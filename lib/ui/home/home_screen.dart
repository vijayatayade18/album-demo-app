import 'package:album/constants/app_colors.dart';
import 'package:album/ui/home/home_tab_view.dart';
import 'package:album/ui/widgets/tab_bar_controller/index.dart';
import 'package:flutter/material.dart';

class HomeScreen extends StatefulWidget {
  @override
  _HomeScreenState createState() => _HomeScreenState();
}

class _HomeScreenState extends State<HomeScreen> {
  @override
  void initState() {
    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return Material(
      type: MaterialType.transparency,
      child: Container(
        color: AppColors.appBgColor,
        child: HomeTabView(
          tabIndexController: TabIndexController(0),
          onTabChange: () {},
        ),
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
  }
}

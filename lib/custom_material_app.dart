import 'package:flutter/material.dart';

class CustomLocalizedMaterialApp extends StatelessWidget {
  final ThemeData theme;
  final Widget home;
  final List<NavigatorObserver> navigatorObservers;

  CustomLocalizedMaterialApp(
    this.home, {
    this.theme,
    this.navigatorObservers = const <NavigatorObserver>[],
  });

  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      navigatorObservers: navigatorObservers,
      builder: (context, child) {
        return MediaQuery(
          child: child,
          data: MediaQuery.of(context).copyWith(textScaleFactor: 1.0),
        );
      },
      theme: theme,
      home: home,
    );
  }
}

import 'package:album/constants/index.dart';
import 'package:album/frameworks/app_theme/index.dart';
import 'package:album/frameworks/controller/edit_text_controller.dart';
import 'package:album/inherited_widgets/theme_provider.dart';
import 'package:album/utils/index.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class MultiLineEditText extends StatefulWidget {
  final Widget prefix;
  final Widget suffix;
  final String placeHolder;
  final String label;
  final bool secureText;
  final bool required;
  final bool autoCorrect;
  final Widget errorView;
  final int numberOfLines;
  final int maxLength;
  final String errorMessage;
  final ValueChanged<String> onTextChanged;
  final VoidCallback onEditingComplete;
  final EditTextController textController;
  final TextInputType textInputType;
  final TextInputAction textInputAction;
  final List<TextInputFormatter> inputFormatters;
  final double textFieldTopSpace;
  final double textFieldBottomSpace;
  final Color cursorColor;
  final double bottomBarHeight;
  final bool enabled;
  final bool scrollEnabled;
  final double minHeight;
  final bool showCounter;
  final bool autoFocus;

  const MultiLineEditText({
    Key key,
    this.prefix,
    this.suffix,
    this.placeHolder,
    this.label,
    this.secureText = false,
    this.required = false,
    this.autoCorrect = true,
    this.errorView,
    this.numberOfLines,
    this.maxLength,
    this.onTextChanged,
    this.onEditingComplete,
    this.textController,
    this.textInputType = TextInputType.multiline,
    this.textInputAction,
    this.inputFormatters,
    this.textFieldTopSpace = 2.0,
    this.textFieldBottomSpace = 2.0,
    this.errorMessage,
    this.cursorColor = Colors.white,
    this.bottomBarHeight = 1,
    this.enabled = true,
    this.scrollEnabled = true,
    this.minHeight = 50.0,
    this.showCounter = false,
    this.autoFocus = false,
  }) : super(key: key);

  @override
  _MultiLineEditTextState createState() => _MultiLineEditTextState();
}

class _MultiLineEditTextState extends State<MultiLineEditText> with TickerProviderStateMixin {
  bool get isError => (widget.textController.error != null);
  FocusNode _focusNode = FocusNode();
  AnimationController _animController;
  String placeHolder;
  Color bottomColor;
  AppThemeData appThemeData;

  ValueNotifier<int> _textLength = ValueNotifier<int>(0);

  ValueNotifier<int> get textLength => _textLength;

  @override
  void initState() {
    WidgetsBinding.instance.addPostFrameCallback(_afterLayout);
    _textLength.value = widget.textController.value.text.length;
    super.initState();
//    placeHolder = widget.label + (widget.required ? ' *' : '');
    placeHolder = widget.placeHolder;

    _animController = AnimationController(vsync: this, duration: Duration(milliseconds: 200));
    _focusNode.addListener(__onFocusChange);
  }

  void _afterLayout(_) {}

  void __onFocusChange() {
    widget.textController.addListener(_addText);
    String tempString = widget.textController.text.toString();

    setState(() {
      if (_focusNode.hasFocus) {
        bottomColor = appThemeData.multiLineEditTextTheme.focusColor;
      } else if (isError) {
        bottomColor = appThemeData.multiLineEditTextTheme.errorColor;
      } else {
        bottomColor = appThemeData.multiLineEditTextTheme.normalColor;
      }
    });

    if (tempString.isNotEmpty && _focusNode.hasFocus) {
      setState(() {
        placeHolder = widget.placeHolder;
        _animController.forward();
      });
    } else if (tempString.isEmpty && _focusNode.hasFocus) {
      setState(() {
        placeHolder = widget.placeHolder;
        _animController.forward();
      });
    } else if (tempString.isEmpty && !_focusNode.hasFocus) {
      setState(() {
        placeHolder = widget.placeHolder;
        _animController.reverse();
      });
    } else if (tempString.isNotEmpty && !_focusNode.hasFocus) {
      setState(() {
        placeHolder = widget.placeHolder;
        _animController.forward();
      });
    }
  }

  @override
  void dispose() {
    _focusNode.removeListener(__onFocusChange);
    widget.textController.removeListener(_addText);
    _animController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    appThemeData = ThemeProvider.of(context);

    isError ? bottomColor = appThemeData.multiLineEditTextTheme.errorColor : bottomColor = appThemeData.multiLineEditTextTheme.normalColor;

    return Container(
      child: Column(
        children: UIUtil.getSeparatedWidgets([
          _getTextFiledContainer(),
          widget.textController.error!=null ? _errorView() : null,
        ], interItemSpace: widget.textFieldBottomSpace, flowHorizontal: false),
      ),
    );
  }

  Container _errorView() {
    return Container(
      alignment: Alignment.centerLeft,
      padding: EdgeInsets.only(left: widget.prefix != null ? 32 : 0),
      child: widget.errorView,
    );
  }

  // ignore: always_declare_return_types
  _addText() {
//    var text = widget.textController.text;
  }

  Widget _getEditTextField() {
    return Expanded(
        child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: UIUtil.getSeparatedWidgets(
              [
                Row(
                  children: <Widget>[
                    (widget.label.isNotEmpty)
                        ? Text(
                            widget.label + (widget.required ? ' *' : ''),
                            style: (widget.enabled) ? appThemeData.multiLineEditTextTheme.enabledLabelStyle : appThemeData.multiLineEditTextTheme.disableLabelStyle,
                          )
                        : Container(),
                  ],
                ),
                Container(
                  decoration: BoxDecoration(shape: BoxShape.rectangle, borderRadius: BorderRadius.circular(4.0), color: AppColors.textFieldBgColor, boxShadow: <BoxShadow>[
                    BoxShadow(
                      color: Color(0x29000000),
                      offset: Offset(0.0, 2.0),
                      blurRadius: 2,
                      spreadRadius: 0.0,
                    )
                  ]),
                  child: Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      _getTextField(),
                      (widget.showCounter)
                          ? Container(
                              padding: EdgeInsets.all(8),
                              child: Row(
                                mainAxisAlignment: MainAxisAlignment.end,
                                children: <Widget>[
                                  Container(
                                    child: ValueListenableBuilder<int>(
                                      valueListenable: textLength,
                                      builder: (context, length, _) {
                                        return Text(length.toString() + '/' + widget.maxLength.toString(), style: appThemeData.multiLineEditTextTheme.enabledPlaceHolderStyle);
                                      },
                                    ),
                                  )
                                ],
                              ),
                            )
                          : Container()
                    ],
                  ),
                ),
              ],
              interItemSpace: widget.textFieldTopSpace,
              flowHorizontal: false,
            )));
  }

  Container _getScrollableTextField() {
    return Container(
        height: 100,
        padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
        child: SingleChildScrollView(
          child: Container(
              alignment: Alignment.centerLeft,
              child: Text(
                widget.textController.text,
                style: (widget.enabled) ? appThemeData.multiLineEditTextTheme.enabledTextStyle : appThemeData.multiLineEditTextTheme.disableTextStyle,
              )),
        ));
  }

  Container _getEditableTextField() {
    return Container(
        height: 100,
        padding: EdgeInsets.symmetric(vertical: 0, horizontal: 10),
        child: TextField(
          focusNode: _focusNode,
          enabled: widget.enabled,
          controller: widget.textController,
          maxLines: widget.numberOfLines,
          obscureText: widget.secureText,
          autocorrect: widget.autoCorrect,
          keyboardType: widget.textInputType,
          autofocus: widget.autoFocus,
          textInputAction: widget.textInputAction,
          textAlign: TextAlign.left,
          style: (widget.enabled) ? appThemeData.multiLineEditTextTheme.enabledTextStyle : appThemeData.multiLineEditTextTheme.disableTextStyle,
          onChanged: (text) {
            _textLength.value = text.length;
            widget.textController.error = null;
            if (widget.onTextChanged != null) {
              widget.onTextChanged(text);
            }
          },
          onEditingComplete: widget.onEditingComplete,
          inputFormatters: widget.inputFormatters,
          scrollPadding: EdgeInsets.zero,
          cursorColor: widget.cursorColor,
          decoration: InputDecoration(hintText: placeHolder, counterText: '', hintStyle: (widget.enabled) ? appThemeData.multiLineEditTextTheme.enabledPlaceHolderStyle : appThemeData.multiLineEditTextTheme.disablePlaceHolderStyle, border: InputBorder.none, contentPadding: EdgeInsets.zero),
        ));
  }

  Widget _getTextFiledContainer() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: [
        Row(
            children: UIUtil.getSeparatedWidgets(
          [
            widget.prefix,
            SizedBox(
              width: widget.prefix != null ? 10.0 : 0,
            ),
            _getEditTextField(),
            widget.suffix,
          ],
        )),
        if (widget.textController.error!=null)
          Container(
            color: AppColors.red,
            height: 2,
          ),
      ],
    );
  }

  Container _getTextField() {
    if (widget.enabled) {
      return _getEditableTextField();
    } else if (!widget.enabled && !widget.scrollEnabled) {
      return Container(
          alignment: Alignment.centerLeft,
          padding: EdgeInsets.symmetric(vertical: 10, horizontal: 10),
          constraints: BoxConstraints(minHeight: widget.minHeight),
          child: Text(
            widget.textController.text,
            style: (widget.enabled) ? appThemeData.multiLineEditTextTheme.enabledTextStyle : appThemeData.multiLineEditTextTheme.disableTextStyle,
          ));
    } else {
      return _getScrollableTextField();
    }
  }
}

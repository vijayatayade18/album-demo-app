import 'package:album/constants/app_colors.dart';
import 'package:album/frameworks/app_theme/index.dart';
import 'package:album/frameworks/controller/index.dart';
import 'package:album/inherited_widgets/theme_provider.dart';
import 'package:album/utils/index.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

class CustomTextField extends StatefulWidget {
  final Widget prefix;
  final Widget suffix;
  final String placeHolder;
  final String label;
  final bool secureText;
  final bool required;
  final bool autoCorrect;
  final Widget errorView;
  final int numberOfLines;
  final int maxLength;
  final String errorMessage;
  final ValueChanged<String> onTextChanged;
  final VoidCallback onEditingComplete;
  final EditTextController textController;
  final TextInputType textInputType;
  final TextInputAction textInputAction;
  final List<TextInputFormatter> inputFormatters;
  final double textFieldTopSpace;
  final double textFieldBottomSpace;
  final Color cursorColor;
  final double bottomBarHeight;
  final bool enabled;
  final bool autoFocus;
  final Widget micStatusWidget;

  const CustomTextField(
      {Key key,
      this.prefix,
      this.suffix,
      this.placeHolder,
      this.label,
      this.secureText = false,
      this.required = false,
      this.autoCorrect = true,
      this.errorView,
      this.numberOfLines = 1,
      this.maxLength,
      this.onTextChanged,
      this.onEditingComplete,
      this.textController,
      this.textInputType = TextInputType.text,
      this.textInputAction = TextInputAction.go,
      this.inputFormatters,
      this.textFieldTopSpace = 4.0,
      this.textFieldBottomSpace = 2.0,
      this.errorMessage,
      this.cursorColor = Colors.black,
      this.bottomBarHeight = 1,
      this.enabled = true,
      this.autoFocus = false,
      this.micStatusWidget})
      : super(key: key);

  @override
  _CustomTextFieldState createState() => _CustomTextFieldState();
}

class _CustomTextFieldState extends State<CustomTextField> with TickerProviderStateMixin {
  String placeHolder;
  Color bottomColor;
  AppThemeData appThemeData;

  bool get isError => (widget.errorView != null);
  TextStyle labelTextStyle;

  @override
  void initState() {
    widget.textController.focusNode = FocusNode();
    WidgetsBinding.instance.addPostFrameCallback(_afterLayout);
    placeHolder = widget.label + (widget.required ? ' *' : '');
    widget.textController.focusNode.addListener(_onFocusChange);
    super.initState();
  }

  void _onFocusChange() {
    if (widget.enabled && widget.textController.focusNode.hasFocus) {
      setState(() {
        labelTextStyle = appThemeData.editTextTheme.enabledLabelStyle.copyWith(color: appThemeData.editTextTheme.focusColor);
      });
    } else if (widget.enabled && !widget.textController.focusNode.hasFocus) {
      setState(() {
        labelTextStyle = appThemeData.editTextTheme.enabledLabelStyle;
      });
    }
  }

  void _afterLayout(_) {
    labelTextStyle = (widget.enabled) ? appThemeData.editTextTheme.enabledLabelStyle : appThemeData.editTextTheme.disableLabelStyle;
  }

  @override
  Widget build(BuildContext context) {
    appThemeData = ThemeProvider.of(context);
    labelTextStyle = (widget.enabled) ? appThemeData.editTextTheme.enabledLabelStyle : appThemeData.editTextTheme.disableLabelStyle;
    if (widget.enabled && widget.textController.focusNode.hasFocus) {
      labelTextStyle = appThemeData.editTextTheme.enabledLabelStyle.copyWith(color: appThemeData.editTextTheme.focusColor);
    } else if (widget.enabled && !widget.textController.focusNode.hasFocus) {
      labelTextStyle = appThemeData.editTextTheme.enabledLabelStyle;
    }
    isError ? bottomColor = appThemeData.editTextTheme.errorColor : bottomColor = appThemeData.editTextTheme.normalColor;
    return Material(
      type: MaterialType.transparency,
      child: Container(
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            _labelContainer(),
            Container(
              margin: EdgeInsets.only(top: 10, left: 0, right: 0, bottom: 0),
              decoration: BoxDecoration(shape: BoxShape.rectangle, borderRadius: BorderRadius.circular(4.0), color: AppColors.textFieldBgColor, boxShadow: <BoxShadow>[
                BoxShadow(
                  color: Color(0x29000000),
                  offset: Offset(0.0, 2.0),
                  blurRadius: 2,
                  spreadRadius: 0.0,
                )
              ]),
              child: Column(
                crossAxisAlignment: CrossAxisAlignment.start,
                mainAxisSize: MainAxisSize.max,
                children: <Widget>[
                  Row(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    mainAxisSize: MainAxisSize.min,
                    children: <Widget>[
                      widget.prefix != null ? widget.prefix : Container(),
                      SizedBox(
                        width: widget.prefix == null ? 0.0 : 10.0,
                      ),
                      Expanded(
                        child: Container(
                          margin: EdgeInsets.symmetric(vertical: 4),
                          child: Column(
                            crossAxisAlignment: CrossAxisAlignment.start,
                            mainAxisSize: MainAxisSize.min,
                            children: UIUtil.getSeparatedWidgets(<Widget>[
                              Container(
                                height: widget.textFieldTopSpace,
                              ),
                              _actualTextContainer(),
                              Container(
                                height: widget.textFieldBottomSpace,
                              )
                            ]),
                          ),
                        ),
                      )
                    ],
                  ),
                  if (widget.errorView != null)
                    Container(
                      color: bottomColor,
                      height: 2,
                    )
                ],
              ),
            ),
            (widget.errorView != null) ? Container(padding: EdgeInsets.only(top: 4), child: widget.errorView) : Container()
          ],
        ),
      ),
    );
  }

  Widget _actualTextContainer() {
    return Container(
      padding: EdgeInsets.only(left: 12.0),
      child: Row(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          Expanded(
            child: Container(
              alignment: Alignment.bottomLeft,
              child: Container(
                margin: EdgeInsets.all(8),
                child: Row(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  mainAxisAlignment: MainAxisAlignment.start,
                  children: <Widget>[
                    Expanded(
                      child: TextField(
                        autofocus: widget.autoFocus,
                        focusNode: widget.textController.focusNode,
                        enabled: widget.enabled,
                        controller: widget.textController,
                        maxLines: widget.numberOfLines,
                        maxLength: widget.maxLength,
                        obscureText: widget.secureText,
                        autocorrect: widget.autoCorrect,
                        keyboardType: widget.textInputType,
                        textInputAction: widget.textInputAction,
                        style: ((widget.enabled) ? appThemeData?.editTextTheme?.enabledTextStyle : appThemeData?.editTextTheme?.disableTextStyle),
                        onChanged: (text) {
                          widget.textController.error = null;
                          if (widget.textController.onTextChanged != null) {
                            widget.textController.onTextChanged(text);
                          }
                          if (widget.onTextChanged != null) {
                            widget.onTextChanged(text);
                          }
                        },
                        onEditingComplete: widget.onEditingComplete,
                        inputFormatters: widget.inputFormatters,
                        scrollPadding: EdgeInsets.zero,
                        cursorColor: widget.cursorColor,
                        decoration: InputDecoration(
                          counterText: '',
                          isDense: true,
                          hintText: widget.placeHolder,
                          hintStyle: ((widget.enabled) ? appThemeData?.editTextTheme?.enabledPlaceHolderStyle : appThemeData?.editTextTheme?.disablePlaceHolderStyle),
                          border: InputBorder.none,
                          contentPadding: EdgeInsets.only(left: 0, right: 8, top: -4, bottom: 0),
                        ),
                      ),
                    )
                  ],
                ),
              ),
            ),
          ),
          (widget.suffix != null && widget.enabled) ? widget.suffix : Container()
        ],
      ),
    );
  }

  Container _labelContainer() {
    return widget.label != null
        ? Container(
            child: Text(
              widget.label,
              style: labelTextStyle,
            ),
          )
        : Container();
  }
}

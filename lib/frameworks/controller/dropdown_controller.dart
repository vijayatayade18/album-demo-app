import 'package:flutter/material.dart';

abstract class DropdownController {
  String get title;

  Listenable get listenable;

  String get error;

  bool get enabled;

  bool get highlight;
}

import 'package:flutter/material.dart';

import 'index.dart';

abstract class AppThemeData {
  EditTextTheme editTextTheme;
  Color backgroundColor;
  MultiLineEditTextTheme multiLineEditTextTheme;
  TabBarThemeData tabBarTheme;

  ThemeData toTheme() {
    return ThemeData(backgroundColor: backgroundColor);
  }
}

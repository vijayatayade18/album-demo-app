import 'package:flutter/material.dart';

abstract class TabBarThemeData {
  Color highlightedTintColor;
  Color tintColor;
  TextStyle selectedTextStyle;
  TextStyle textStyle;
  Gradient selectedGradient;
}

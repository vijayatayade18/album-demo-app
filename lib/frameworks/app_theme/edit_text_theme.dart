import 'package:flutter/material.dart';

abstract class EditTextTheme {
  TextStyle enabledLabelStyle;
  TextStyle enabledPlaceHolderStyle;
  TextStyle enabledTextStyle;
  TextStyle disableLabelStyle;
  TextStyle disablePlaceHolderStyle;
  TextStyle disableTextStyle;
  Color focusColor;
  Color normalColor;
  Color errorColor;
  Color alabaster;
  Color disableColor;
  Color enabledColor;
  Color cursorColor;
}

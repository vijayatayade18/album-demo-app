class AppStrings {
  static String errorMessage = "Something went wrong, please try again later.";
  static String logIn = "LOG IN";
  static String labelEmailId = "Email ID";
  static String labelPassword = "Password";
  static String btnContinue = "Continue";
  static String placeholderEmailID = "Please enter email id";
  static String placeholderPassword = "Please enter password";

  static String labelBasicInformation = "BASIC INFORMATION";
  static String labelName = "Name";
  static String labelEmailAddress = "Email Address";
  static String labelPhoneNo = "Phone No";
  static String labelWebsite = "Website";
  static String labelCompany = "Company";
  static String labelAddressInformation = "ADDRESS INFORMATION";
  static String labelStreet = "Street";
  static String labelSuit = "Suit";
  static String labelCity = "City";
  static String labelZipcode = "Zipcode";
  static String labelLogout = "LOGOUT";
  static String labelUserProfile = "User Profile";
  static String labelUserName = "Username";

  static String labelAlbum = "Album";

  static String labelTodoList = "To Do List";
  static String btnViewComments = "View Comments";
  static String titleCreatePost = "Create Posts";
  static String labelTitle = "Title";
  static String labelPost = "Post";
  static String btnSubmitPost = "Submit Post";
  static String placeholderTitle = "Please enter title";
  static String placeholderPost = "Please enter post";
  static String createPostLoadingMessage = "Creating Post";

  static String btnOkay = "Okay";
  static String titlePosts = "Posts";

  static String btnRetry = "Retry";

  static String noAlbumsFound = "No albums found.";

  static String noPhotosFound = "Album photos are not available.";

  static String noCommentsFound = "No comments available";

  static String noPostFound = "No posts available";

  static String noTodosFound = "No todos available";

  static String completed = "Completed";
  static String inCompleted = "incomplete";

  static String titleViewComments = "View Comments";

  static String tabHome = "Home";
  static String tabAlbum = "Album";
  static String tabToDoList = "To Do List";
  static String tabProfile = "Profile";

  static String errorLoginMessage = "Please enter the correct credentials.";

  static String loaderTextVerifyingUser = "Verifying user";

  static String errorEmptyUsername = "Email id must not be empty";
  static String errorEmptyPassword = "Password must not be empty";
}

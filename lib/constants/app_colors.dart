import 'package:flutter/material.dart';

class AppColors {
  static const MaterialColor primarySwatch = const MaterialColor(0xffe55f48, // 0% comes in here, this will be color picked if no shade is selected when defining a Color property which doesn’t require a swatch.
      const <int, Color>{
        50: const Color(0xffce5641), //10%
        100: const Color(0xffb74c3a), //20%
        200: const Color(0xffa04332), //30%
        300: const Color(0xff89392b), //40%
        400: const Color(0xff733024), //50%
        500: const Color(0xff5c261d), //60%
        600: const Color(0xff451c16), //70%
        700: const Color(0xff2e130e), //80%
        800: const Color(0xff170907), //90%
        900: const Color(0xff000000),
      });

  static const primaryColor = const Color(0xffce5641);
  static const appBgColor = const Color(0xff121212);
  static const shimmerColor = const Color(0xFFDDDDDD);
  static const white = Colors.white;
  static const greenColor = const Color(0xff5DB65F);
  static const textFieldBgColor = const Color(0xff414141);

  static const red = Color(0xFFFF0000);

  static const appbarBgColor= const Color(0xff191919);
  static const tabBarBgColor= const Color(0xff272828);
  static const listCardBgColor= const Color(0xff191919);

}

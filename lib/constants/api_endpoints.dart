import 'package:album/utils/index.dart';

class ApiEndPoints {
  static String get baseUrl => "https://jsonplaceholder.typicode.com/";

  static String get posts => "posts?userId=${SharedPreferencesUtil.getUserId()}";

  static String get albums => "albums?userId=${SharedPreferencesUtil.getUserId()}";

  static String albumPhotos(int albumId) => "photos?userId=${SharedPreferencesUtil.getUserId()}&albumId=$albumId";

  static String get todos => "todos?userId=${SharedPreferencesUtil.getUserId()}";

  static String createPost = "posts";

  static String viewComments(int postId) => "comments?postId=$postId";
  static String users = "users";
}

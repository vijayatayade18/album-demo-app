import 'package:album/constants/app_colors.dart';
import 'package:flutter/material.dart';

class TextStyles {
  static const String poppins = 'Poppins';

  static const FontWeight light = FontWeight.w300;
  static const FontWeight regular = FontWeight.w400;
  static const FontWeight medium = FontWeight.w500;
  static const FontWeight bold = FontWeight.w700;
  static const FontWeight semiBold = FontWeight.w600;

  static TextStyle get errorTextStyle => TextStyle(
        inherit: false,
        color: AppColors.red,
        fontWeight: regular,
        fontFamily: poppins,
        fontSize: 14.0,
      );

  static TextStyle get retryButtonTextStyle => TextStyle(
        inherit: false,
        color: Colors.black,
        fontWeight: medium,
        fontFamily: poppins,
        fontSize: 12.0,
      );

  static TextStyle get appBarTitle => TextStyle(
        inherit: false,
        color: Colors.white,
        fontWeight: medium,
        fontFamily: poppins,
        fontSize: 18.0,
      );

  static TextStyle get appBarSubTitle => TextStyle(
        inherit: false,
        color: AppColors.white.withOpacity(
          0.5,
        ),
        fontWeight: regular,
        fontFamily: poppins,
        fontSize: 16.0,
      );

  static const TextStyle inputFieldLabel = TextStyle(
    // inherit: false,
    color: Colors.white,
    fontWeight: bold,
    fontFamily: poppins,
    fontSize: 16.0,
  );

  static const TextStyle inputFieldPlaceHolder = TextStyle(
    // inherit: false,
    color: Color(0xffA0A0A0),
    fontWeight: regular,
    fontFamily: poppins,
    fontSize: 16.0,
  );

  static const TextStyle inputFieldText = TextStyle(
    // inherit: false,
    color: Color(0xffA0A0A0),
    fontWeight: medium,
    fontFamily: poppins,
    fontSize: 16.0,
  );

  static const TextStyle disableInputFieldLabel = TextStyle(
    // inherit: false,
    color: Colors.white,
    fontWeight: bold,
    fontFamily: poppins,
    fontSize: 18.0,
  );

  static const TextStyle disableInputFieldPlaceHolder = TextStyle(
    // inherit: false,
    color: Color(0xffA0A0A0),
    fontWeight: regular,
    fontFamily: poppins,
    fontSize: 16.0,
  );

  static const TextStyle disableInputFieldText = TextStyle(
    // inherit: false,
    color: Colors.white,
    fontWeight: bold,
    fontFamily: poppins,
    fontSize: 18.0,
  );

  static const TextStyle logInLabelStyle = TextStyle(
    inherit: false,
    color: Colors.white,
    fontWeight: medium,
    fontFamily: poppins,
    fontSize: 14.0,
  );

  static const TextStyle primaryButtonStyle = TextStyle(
    inherit: false,
    color: Colors.white,
    fontWeight: medium,
    fontFamily: poppins,
    fontSize: 14.0,
  );

  static TextStyle get postTitle => TextStyle(
        inherit: false,
        color: Colors.white,
        fontWeight: medium,
        fontFamily: poppins,
        fontSize: 16.0,
      );

  static TextStyle get postDescription => TextStyle(
        inherit: false,
        color: Colors.white.withOpacity(0.5),
        fontWeight: regular,
        fontFamily: poppins,
        fontSize: 14.0,
      );

  static TextStyle get viewCommentLabel => TextStyle(
        inherit: false,
        color: Colors.white,
        fontWeight: medium,
        fontFamily: poppins,
        fontSize: 14.0,
        decoration: TextDecoration.underline,
      );

  static TextStyle get profileInformationHeader => TextStyle(
        inherit: false,
        color: Colors.white.withOpacity(
          0.5,
        ),
        fontWeight: medium,
        fontFamily: poppins,
        fontSize: 14.0,
      );

  static TextStyle get profileInformationTitle => TextStyle(
        inherit: false,
        color: Colors.white,
        fontWeight: medium,
        fontFamily: poppins,
        fontSize: 14.0,
      );

  static TextStyle get profileInformationValue => TextStyle(
        inherit: false,
        color: Colors.white.withOpacity(
          0.5,
        ),
        fontWeight: medium,
        fontFamily: poppins,
        fontSize: 14.0,
      );

  static TextStyle get albumCardText => TextStyle(
        inherit: false,
        color: Colors.white,
        fontWeight: medium,
        fontFamily: poppins,
        fontSize: 18.0,
      );

  static TextStyle todoStatus(bool isCompleted) => TextStyle(
        inherit: false,
        color: isCompleted ? AppColors.greenColor : AppColors.red,
        fontWeight: medium,
        fontFamily: poppins,
        fontSize: 14.0,
      );

  static TextStyle get tabBarTextSelected => TextStyle(
        inherit: false,
        color: AppColors.white,
        fontWeight: medium,
        fontFamily: poppins,
        fontSize: 14.0,
      );

  static TextStyle get tabBarTextUnSelected => TextStyle(
        inherit: false,
        color: AppColors.white.withOpacity(
          0.5,
        ),
        fontWeight: regular,
        fontFamily: poppins,
        fontSize: 14.0,
      );

  static TextStyle get dialogErrorTextStyle => TextStyle(
        inherit: false,
        color: AppColors.appBgColor,
        fontWeight: regular,
        fontFamily: poppins,
        fontSize: 14.0,
      );

  static TextStyle get onScreenErrorTextStyle => TextStyle(
        inherit: false,
        color: AppColors.white,
        fontWeight: regular,
        fontFamily: poppins,
        fontSize: 14.0,
      );
}

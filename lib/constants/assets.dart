class Assets {
  static String gridIcon = 'assets/images/ic_grid.png';
  static String listIcon = 'assets/images/ic_list.png';
  static String releaseDate = 'assets/images/release_date.png';
  static String language = 'assets/images/language.png';
  static String rating = 'assets/images/rating.png';
  static String error = 'assets/images/error.png';
  static String userProfile = 'assets/images/user.png';
  static String farwordArrow = 'assets/images/forword_arrow.svg';
  static String tabHomeSelected = 'assets/images/tab_home_selected.svg';
  static String tabHomeDeSelected = 'assets/images/tab_home_deselected.svg';
  static String tabProfileDeselected = 'assets/images/tab_profile_deselected.svg';
  static String tabProfileSelected = 'assets/images/tab_profile_selected.svg';
  static String tabToDoListDeselected = 'assets/images/tab_todo_list_deselected.svg';
  static String tabToDoListSelected = 'assets/images/tab_todo_list_selected.svg';
  static String tabAlbumSelected = 'assets/images/tab_album_selected.svg';
  static String tabAlbumDeselected = 'assets/images/tab_album_deselected.svg';
  static String appLogo = 'assets/images/app_logo.svg';
  static String fabIcon = 'assets/images/fab_button.svg';
  static String backArrow = 'assets/images/back_arrow.svg';
}

import 'package:album/blocs/index.dart';
import 'package:album/constants/app_colors.dart';
import 'package:album/custom_material_app.dart';
import 'package:album/inherited_widgets/index.dart';
import 'package:album/inherited_widgets/theme_provider.dart';
import 'package:album/ui/home/home_screen.dart';
import 'package:album/ui/login/login_screen.dart';
import 'package:album/utils/index.dart';
import 'package:album/utils/shared_preferences_handler.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';

enum AppState { LOGGED_IN, NOT_LOGGED_IN }

ValueNotifier<bool> isInitialized = ValueNotifier(false);
LoginBloc _loginBloc = LoginBloc();
ValueNotifier<AppState> currentAppState = ValueNotifier(AppState.NOT_LOGGED_IN);

void main() async {
  SystemChrome.setSystemUIOverlayStyle(SystemUiOverlayStyle(
    statusBarColor: AppColors.appbarBgColor, // status bar color
  ));
  WidgetsFlutterBinding.ensureInitialized();
  await setupLocator();
  await SharedPreferencesHandler.initialize();
  await _loginBloc.getUsers();
  var userDetails = _loginBloc.getUserDetails();
  if (userDetails != null) {
    currentAppState.value = AppState.LOGGED_IN;
  } else {
    currentAppState.value = AppState.NOT_LOGGED_IN;
  }
  runApp(AlbumApp());
  isInitialized.value = true;
}

class AlbumApp extends StatefulWidget {
  @override
  _AlbumAppState createState() => _AlbumAppState();
}

class _AlbumAppState extends State<AlbumApp> {
  AlbumAppThemeData theme = AlbumAppThemeData();

  @override
  Widget build(BuildContext context) {
    return ValueListenableBuilder(
        valueListenable: isInitialized,
        builder: (context, isInitialized, __) {
          if (isInitialized) {
            return ThemeProvider(
              theme: theme,
              child: ValueListenableBuilder(
                valueListenable: currentAppState,
                builder: (context, appState, __) {
                  return CustomLocalizedMaterialApp(
                    appState == AppState.NOT_LOGGED_IN ? LoginScreen() : HomeScreen(),
                    theme: theme?.toTheme(),
                  );
                },
              ),
            );
          } else {
            return Container();
          }
        });
  }
}

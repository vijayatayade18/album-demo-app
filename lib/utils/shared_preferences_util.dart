import 'package:album/constants/index.dart';
import 'package:album/utils/shared_preferences_handler.dart';

class SharedPreferencesUtil {
  static int getUserId() {
    return SharedPreferencesHandler.instance.getInt(
      SharedPrefConstants.USER_ID,
    );
  }
}

import 'dart:io';

import 'package:album/utils/index.dart';

class DownloadUtil {
  static Future<File> download(String url) async {
    File cachedFile = (await ImageCacheManager().getFileFromCache(url))?.file;
    if (cachedFile == null) {
      cachedFile = (await ImageCacheManager().downloadFile(url))?.file;
    }
    return cachedFile;
  }
}

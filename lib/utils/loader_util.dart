import 'package:album/constants/index.dart';
import 'package:album/ui/widgets/buttons/index.dart';
import 'package:album/utils/index.dart';
import 'package:flutter/material.dart';
import 'package:flutter_svg/flutter_svg.dart';

class DialogUtil {
  static showLoader(BuildContext context, String loadingText) {
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        return Dialog(
            child: Container(
          padding: EdgeInsets.all(20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisAlignment: MainAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: UIUtil.getSeparatedWidgets(
              [
                SvgPicture.asset(
                  Assets.appLogo,
                  height: 100,
                  width: 100,
                ),
                Container(
                  alignment: Alignment.center,
                  child: Row(
                      mainAxisSize: MainAxisSize.min,
                      mainAxisAlignment: MainAxisAlignment.center,
                      crossAxisAlignment: CrossAxisAlignment.center,
                      children: UIUtil.getSeparatedWidgets(
                        [
                          _circularLoader(),
                          Text(
                            loadingText,
                            maxLines: 2,
                            overflow: TextOverflow.ellipsis,
                            textAlign: TextAlign.center,
                          ),
                        ],
                        interItemSpace: 12,
                      )),
                ),
              ],
              interItemSpace: 12,
              flowHorizontal: false,
            ),
          ),
        ));
      },
    );
  }

  static Container _circularLoader() {
    return Container(
      height: 20,
      width: 20,
      child: CircularProgressIndicator(
        strokeWidth: 2,
        valueColor: AlwaysStoppedAnimation<Color>(AppColors.appBgColor),
      ),
    );
  }

  static hideLoader(BuildContext context) {
    Navigator.of(context, rootNavigator: true).pop('dialog');
  }

  static showErrorDialog(BuildContext context, String errorText) {
    showDialog(
      context: context,
      barrierDismissible: true,
      builder: (BuildContext context) {
        return Dialog(
            child: Container(
          padding: EdgeInsets.all(20),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.center,
            mainAxisSize: MainAxisSize.min,
            children: UIUtil.getSeparatedWidgets(
              [
                Image.asset(
                  Assets.error,
                  height: 70,
                  width: 70,
                  color: AppColors.red,
                ),
                Text(
                  errorText,
                  textAlign: TextAlign.center,
                  style: TextStyles.dialogErrorTextStyle,
                ),
                AppButton(
                  width: 140,
                  onPressed: () {
                    hideLoader(context);
                  },
                  title: AppStrings.btnOkay,
                )
              ],
              interItemSpace: 12,
              flowHorizontal: false,
            ),
          ),
        ));
      },
    );
  }
}

import 'package:shared_preferences/shared_preferences.dart';

class SharedPreferencesHandler {
  static SharedPreferences _sharedPreferences;
  static SharedPreferencesHandler _instance;

  static Future<void> initialize() async {
    if (_instance == null) {
      _instance = SharedPreferencesHandler();
      _sharedPreferences = await SharedPreferences.getInstance();
    }
  }

  static SharedPreferencesHandler get instance => _instance;

  Future<bool> clear() async => await _sharedPreferences.clear();

  Future<bool> remove(String key) => _sharedPreferences.remove(key);

  bool getBool(String key) => _sharedPreferences.getBool(key);

  String getString(String key) => _sharedPreferences.getString(key);

  int getInt(String key) => _sharedPreferences.getInt(key);

  double getDouble(String key) => _sharedPreferences.getDouble(key);

  //setter
  Future<bool> setBool(String key, bool value) =>
      _sharedPreferences.setBool(key, value);

  Future<bool> setString(String key, String value) =>
      _sharedPreferences.setString(key, value);

  Future<bool> setInt(String key, int value) =>
      _sharedPreferences.setInt(key, value);

  Future<bool> setDouble(String key, double value) =>
      _sharedPreferences.setDouble(key, value);
}

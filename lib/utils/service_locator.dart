
import 'package:album/data/local/db/my_db.dart';
import 'package:get_it/get_it.dart';

GetIt locator = GetIt.instance;

Future setupLocator() async {
  locator.registerSingleton(MyDatabase());
}
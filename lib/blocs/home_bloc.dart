import 'package:album/constants/index.dart';
import 'package:album/data/models/comments/comments_details_dto.dart';
import 'package:album/data/models/posts/index.dart';
import 'package:album/data/repo/home_repo/home_repo.dart';
import 'package:album/data/repo/home_repo/home_repo_impl.dart';
import 'package:album/frameworks/controller/index.dart';
import 'package:album/ui/home/events/index.dart';
import 'package:album/ui/home/states/index.dart';
import 'package:album/ui/home/states/view_comments_list_state.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class HomeBloc extends Bloc<HomeEvents, HomeState> {
  HomeRepo _homeRepo = HomeRepoImpl();
  List<PostDetailsDTO> postsList;
  List<CommentsDetailsDTO> commentsList;
  ValueNotifier<String> fabButtonIcon = ValueNotifier(null);
  EditTextController postTitleController = EditTextController();
  EditTextController postController = EditTextController();
  int postId;

  HomeBloc(HomeState initialState, HomeEvents initialEvent, {this.postId}) : super(initialState) {
    this.add(initialEvent);
    fabButtonIcon.value = Assets.gridIcon;
  }

  @override
  Stream<HomeState> mapEventToState(event) async* {
    if (event is LoadingStateEvent) {
      yield* fetchPostsList();
    }

    if (event is ViewCommentsLoadingEvent) {
      yield* fetchComments();
    }

    if (event is PostsStateEvent) {
      yield HomePostsFeedState(postsList);
    }
  }

  Stream<HomeState> fetchPostsList() async* {
    try {
      postsList = await _homeRepo.fetchPostsList();
      if (postsList.isNotEmpty) {
        yield HomePostsFeedState(postsList);
      } else {
        yield HomeErrorState(
          errorMessage: AppStrings.noPostFound,
          icon: Assets.appLogo,
          showRetryButton: false,
        );
      }
    } catch (e) {
      yield HomeErrorState(
        errorMessage: AppStrings.errorMessage,
        icon: Assets.error,
        showRetryButton: true,
        onRetryClicked: () {
          this.add(LoadingStateEvent());
          fetchPostsList();
        },
      );
    }
  }

  Future<void> createPost({
    @required Function onStart,
    @required Function onSuccess,
    @required Function onFailure,
  }) async {
    try {
      if (isFieldsValidationSuccess()) {
        if (onStart != null) {
          onStart();
        }
        await _homeRepo.createPost(postTitleController.text, postController.text);
        if (onSuccess != null) {
          onSuccess();
        }
      }
    } catch (e) {
      if (onFailure != null) {
        onFailure();
      }
    }
  }

  Stream<HomeState> fetchComments() async* {
    try {
      commentsList = await _homeRepo.fetchComments(postId);
      if (commentsList.isNotEmpty) {
        yield ViewCommentsListState(commentsList);
      } else {
        yield HomeErrorState(
          errorMessage: AppStrings.noCommentsFound,
          icon: Assets.appLogo,
          showRetryButton: false,
        );
      }
    } catch (e) {
      yield HomeErrorState(
        errorMessage: AppStrings.errorMessage,
        icon: Assets.error,
        showRetryButton: true,
        onRetryClicked: () {
          this.add(ViewCommentsLoadingEvent());
          fetchComments();
        },
      );
    }
  }

  bool isFieldsValidationSuccess() {
    if (postTitleController.text.isEmpty) {
      postTitleController.error = "Please enter the title";
    }

    if (postController.text.isEmpty) {
      postController.error = "Please enter the post";
    }

    return (postTitleController.error == null && postController.error == null);
  }
}

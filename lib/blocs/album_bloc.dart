import 'package:album/constants/index.dart';
import 'package:album/data/models/albums/album_details_dto.dart';
import 'package:album/data/models/albums/album_photo_details_dto.dart';
import 'package:album/data/repo/album_repo/album_repo.dart';
import 'package:album/data/repo/album_repo/index.dart';
import 'package:album/ui/album/events/index.dart';
import 'package:album/ui/album/states/index.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class AlbumBloc extends Bloc<AlbumEvents, AlbumState> {
  AlbumRepo _albumRepo = AlbumRepoImpl();
  List<AlbumDetailsDTO> albumList;
  List<AlbumPhotoDetails> albumPhotosList;
  int albumId;

  AlbumBloc(
    AlbumState initialState,
    AlbumEvents albumEvents, {
    this.albumId,
  }) : super(initialState) {
    this.add(albumEvents);
  }

  @override
  Stream<AlbumState> mapEventToState(event) async* {
    if (event is AlbumLoadingStateEvent) {
      yield* fetchAlbumList();
    }
    if (event is AlbumPhotosLoadingStateEvent) {
      yield* fetchAlbumPhotos();
    }
    if (event is AlbumListStateEvent) {
      yield AlbumListState(albumList);
    }
  }

  Stream<AlbumState> fetchAlbumList() async* {
    try {
      albumList = await _albumRepo.fetchAlbumsList();
      if (albumList.isNotEmpty) {
        yield AlbumListState(albumList);
      } else {
        yield AlbumErrorState(
          errorMessage: AppStrings.noAlbumsFound,
          icon: Assets.appLogo,
          showRetryButton: false,
        );
      }
    } catch (e) {
      yield AlbumErrorState(
        errorMessage: AppStrings.errorMessage,
        icon: Assets.error,
        showRetryButton: true,
        onRetryClicked: () {
          this.add(AlbumLoadingStateEvent());
          fetchAlbumList();
        },
      );
    }
  }

  Stream<AlbumState> fetchAlbumPhotos() async* {
    try {
      albumPhotosList = await _albumRepo.fetchAlbumsPhotos(albumId);
      if (albumPhotosList.isNotEmpty) {
        yield AlbumPhotoListState(albumPhotosList);
      } else {
        yield AlbumErrorState(
          errorMessage: AppStrings.noPhotosFound,
          icon: Assets.appLogo,
          showRetryButton: false,
        );
      }
    } catch (e) {
      yield AlbumErrorState(
        errorMessage: AppStrings.errorMessage,
        icon: Assets.error,
        showRetryButton: true,
        onRetryClicked: () {
          this.add(AlbumPhotosLoadingStateEvent());
          fetchAlbumList();
        },
      );
    }
  }
}

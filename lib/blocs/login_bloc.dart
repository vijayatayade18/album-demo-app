import 'package:album/constants/index.dart';
import 'package:album/data/models/user/index.dart';
import 'package:album/data/repo/login_repo/index.dart';
import 'package:album/frameworks/controller/index.dart';
import 'package:album/main.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';

class LoginBloc {
  static UserDetailsDTO userDetailsDTO;
  static int userId;
  EditTextController usernameController = EditTextController();
  EditTextController passwordController = EditTextController();
  LoginRepo _loginRepo = LoginRepoImpl();

  Future<void> verifyUser({
    @required Function onStart,
    @required Function onSuccess,
    @required Function(String) onFailure,
  }) async {
    if (isFieldValidationSuccess()) {
      try {
        if (onStart != null) {
          onStart();
        }
        userDetailsDTO = await _loginRepo.verifyUser(
          usernameController.text,
          passwordController.text,
        );

        if (userDetailsDTO != null) {
          userId = userDetailsDTO.id;
          usernameController.clear();
          passwordController.clear();
          if (onSuccess != null) {
            onSuccess();
          }
        } else {
          if (onFailure != null) {
            onFailure(AppStrings.errorLoginMessage);
          }
        }
      } catch (e) {
        if (onFailure != null) {
          onFailure(AppStrings.errorMessage);
        }
      }
    }
  }

  bool isFieldValidationSuccess() {
    if (usernameController.text.isEmpty) {
      usernameController.error = AppStrings.errorEmptyUsername;
    }

    if (passwordController.text.isEmpty) {
      passwordController.error = AppStrings.errorEmptyPassword;
    }

    return (usernameController.error == null && passwordController.error == null);
  }

  Future<void> getUsers() async {
    await _loginRepo.fetchUsers();
  }

  UserDetailsDTO getUserDetails() {
    return _loginRepo.getUserDetails();
  }

  int getUserId() {
    return _loginRepo.getUserID();
  }

  Future<void> logoutUser() async {
    SharedPreferences preferences = await SharedPreferences.getInstance();
    await preferences.remove(SharedPrefConstants.USER_INFO);
    await preferences.remove(SharedPrefConstants.USER_ID);
    currentAppState.value = AppState.NOT_LOGGED_IN;
  }
}

import 'package:album/constants/index.dart';
import 'package:album/data/models/todo/index.dart';
import 'package:album/data/repo/todo_repo/index.dart';
import 'package:album/ui/todo/events/index.dart';
import 'package:album/ui/todo/states/index.dart';
import 'package:flutter_bloc/flutter_bloc.dart';

class TodoBloc extends Bloc<TodoEvents, TodoState> {
  TodoRepo _todoRepo = TodosRepoImpl();
  List<TodoDetailsDTO> todoList;

  TodoBloc(
    TodoState initialState,
  ) : super(initialState) {
    this.add(TodoLoadingStateEvent());
  }

  @override
  Stream<TodoState> mapEventToState(event) async* {
    if (event is TodoLoadingStateEvent) {
      yield* fetchAlbumList();
    }

    if (event is TodoListStateEvent) {
      yield TodoListState(todoList);
    }
  }

  Stream<TodoState> fetchAlbumList() async* {
    try {
      todoList = await _todoRepo.fetchTodosList();
      if (todoList.isNotEmpty) {
        yield TodoListState(todoList);
      } else {
        yield TodoErrorState(
          errorMessage: AppStrings.noTodosFound,
          icon: Assets.appLogo,
          showRetryButton: false,
        );
      }
    } catch (e) {
      yield TodoErrorState(
        errorMessage: AppStrings.errorMessage,
        icon: Assets.error,
        showRetryButton: true,
        onRetryClicked: () {
          this.add(TodoLoadingStateEvent());
          fetchAlbumList();
        },
      );
    }
  }
}

// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'album_details_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

AlbumDetailsDTO _$AlbumDetailsDTOFromJson(Map<String, dynamic> json) {
  return AlbumDetailsDTO(
    id: json['id'] as int,
    userId: json['userId'] as int,
    title: json['title'] as String,
  );
}

Map<String, dynamic> _$AlbumDetailsDTOToJson(AlbumDetailsDTO instance) =>
    <String, dynamic>{
      'id': instance.id,
      'userId': instance.userId,
      'title': instance.title,
    };

import 'package:json_annotation/json_annotation.dart';

part 'album_details_dto.g.dart';

@JsonSerializable(
  explicitToJson: true,
  nullable: true,
  createToJson: true,
)
class AlbumDetailsDTO {
  final int id;
  final int userId;
  final String title;

  AlbumDetailsDTO({
    this.id,
    this.userId,
    this.title,
  });

  factory AlbumDetailsDTO.fromJson(Map<String, dynamic> json) => _$AlbumDetailsDTOFromJson(json);

  Map<String, dynamic> toJson() => _$AlbumDetailsDTOToJson(this);
}

import 'package:json_annotation/json_annotation.dart';
part 'album_photo_details_dto.g.dart';

@JsonSerializable(
  explicitToJson: true,
  nullable: true,
  createToJson: true,
)
class AlbumPhotoDetails {
  final int albumId;
  final int id;
  final String title;
  final String url;
  final String thumbnailUrl;

  AlbumPhotoDetails({
    this.albumId,
    this.id,
    this.title,
    this.url,
    this.thumbnailUrl,
  });

  factory AlbumPhotoDetails.fromJson(Map<String, dynamic> json) => _$AlbumPhotoDetailsFromJson(json);

  Map<String, dynamic> toJson() => _$AlbumPhotoDetailsToJson(this);
}

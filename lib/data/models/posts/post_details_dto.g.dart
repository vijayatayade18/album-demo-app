// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'post_details_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

PostDetailsDTO _$PostDetailsDTOFromJson(Map<String, dynamic> json) {
  return PostDetailsDTO(
    id: json['id'] as int,
    userId: json['userId'] as int,
    title: json['title'] as String,
    body: json['body'] as String,
  );
}

Map<String, dynamic> _$PostDetailsDTOToJson(PostDetailsDTO instance) =>
    <String, dynamic>{
      'id': instance.id,
      'userId': instance.userId,
      'title': instance.title,
      'body': instance.body,
    };

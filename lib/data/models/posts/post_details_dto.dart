import 'package:json_annotation/json_annotation.dart';

part 'post_details_dto.g.dart';

@JsonSerializable(
  explicitToJson: true,
  nullable: true,
  createToJson: true,
)
class PostDetailsDTO {
  final int id;
  final int userId;
  final String title;
  final String body;

  PostDetailsDTO({
    this.id,
    this.userId,
    this.title,
    this.body,
  });

  factory PostDetailsDTO.fromJson(Map<String, dynamic> json) => _$PostDetailsDTOFromJson(json);

  Map<String, dynamic> toJson() => _$PostDetailsDTOToJson(this);
}

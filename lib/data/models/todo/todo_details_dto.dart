import 'package:json_annotation/json_annotation.dart';

part 'todo_details_dto.g.dart';

@JsonSerializable(
  explicitToJson: true,
  nullable: true,
  createToJson: true,
)
class TodoDetailsDTO {
  final int id;
  final int userId;
  final String title;
  final bool completed;

  TodoDetailsDTO({this.id, this.userId, this.title, this.completed});

  factory TodoDetailsDTO.fromJson(Map<String, dynamic> json) => _$TodoDetailsDTOFromJson(json);

  Map<String, dynamic> toJson() => _$TodoDetailsDTOToJson(this);
}

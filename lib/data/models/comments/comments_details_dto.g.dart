// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'comments_details_dto.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

CommentsDetailsDTO _$CommentsDetailsDTOFromJson(Map<String, dynamic> json) {
  return CommentsDetailsDTO(
    postId: json['postId'] as int,
    id: json['id'] as int,
    name: json['name'] as String,
    email: json['email'] as String,
    body: json['body'] as String,
  );
}

Map<String, dynamic> _$CommentsDetailsDTOToJson(CommentsDetailsDTO instance) =>
    <String, dynamic>{
      'postId': instance.postId,
      'id': instance.id,
      'name': instance.name,
      'email': instance.email,
      'body': instance.body,
    };

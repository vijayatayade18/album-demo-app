import 'package:json_annotation/json_annotation.dart';

part 'comments_details_dto.g.dart';

@JsonSerializable(
  explicitToJson: true,
  nullable: true,
  createToJson: true,
)
class CommentsDetailsDTO {
  final int postId;
  final int id;
  final String name;
  final String email;
  final String body;

  CommentsDetailsDTO({
    this.postId,
    this.id,
    this.name,
    this.email,
    this.body,
  });

  factory CommentsDetailsDTO.fromJson(Map<String, dynamic> json) => _$CommentsDetailsDTOFromJson(json);

  Map<String, dynamic> toJson() => _$CommentsDetailsDTOToJson(this);
}

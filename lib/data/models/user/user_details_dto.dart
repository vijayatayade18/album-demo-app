import 'package:json_annotation/json_annotation.dart';

part 'user_details_dto.g.dart';

@JsonSerializable(
  explicitToJson: true,
  nullable: true,
  createToJson: true,
)
class UserDetailsDTO {
  final int id;
  final String name;
  final String username;
  final String email;
  final Address address;
  final String phone;
  final String website;
  final Company company;

  UserDetailsDTO({
    this.id,
    this.name,
    this.username,
    this.email,
    this.address,
    this.phone,
    this.website,
    this.company,
  });

  factory UserDetailsDTO.fromJson(Map<String, dynamic> json) => _$UserDetailsDTOFromJson(json);

  Map<String, dynamic> toJson() => _$UserDetailsDTOToJson(this);
}

@JsonSerializable(
  explicitToJson: true,
  nullable: true,
  createToJson: true,
)
class Address {
  final String street;
  final String suite;
  final String city;
  final String zipcode;
  final Geo geo;

  Address({
    this.street,
    this.suite,
    this.city,
    this.zipcode,
    this.geo,
  });

  factory Address.fromJson(Map<String, dynamic> json) => _$AddressFromJson(json);

  Map<String, dynamic> toJson() => _$AddressToJson(this);
}

@JsonSerializable(
  explicitToJson: true,
  nullable: true,
  createToJson: true,
)
class Geo {
  final String lat;
  final String lng;

  Geo({
    this.lat,
    this.lng,
  });

  factory Geo.fromJson(Map<String, dynamic> json) => _$GeoFromJson(json);

  Map<String, dynamic> toJson() => _$GeoToJson(this);
}

@JsonSerializable(
  explicitToJson: true,
  nullable: true,
  createToJson: true,
)
class Company {
  final String name;
  final String catchPhrase;
  final String bs;

  Company({
    this.name,
    this.catchPhrase,
    this.bs,
  });

  factory Company.fromJson(Map<String, dynamic> json) => _$CompanyFromJson(json);

  Map<String, dynamic> toJson() => _$CompanyToJson(this);
}

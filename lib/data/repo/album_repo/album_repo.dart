import 'package:album/data/models/albums/index.dart';

abstract class AlbumRepo {
  Future<List<AlbumDetailsDTO>> fetchAlbumsList();
  Future<List<AlbumPhotoDetails>> fetchAlbumsPhotos(int albumId);
}

import 'package:album/constants/index.dart';
import 'package:album/data/local/db/my_db.dart';
import 'package:album/data/models/albums/index.dart';
import 'package:album/data/network/index.dart';
import 'package:album/data/repo/album_repo/index.dart';
import 'package:album/utils/index.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';

class AlbumRepoImpl extends AlbumRepo {
  MyDatabase _myDatabase;

  AlbumRepoImpl() {
    _myDatabase = locator.get();
  }

  @override
  Future<List<AlbumDetailsDTO>> fetchAlbumsList() async {
    ApiClient apiClient = ApiClient.public();
    try {
      Response response = await apiClient.dioClient.get(
        ApiEndPoints.albums,
      );

      List<AlbumDetailsDTO> postsList = (response.data as List)?.map((e) => e == null ? null : AlbumDetailsDTO.fromJson(e as Map<String, dynamic>))?.toList();
      debugPrint("albums length: ${postsList.length}");
      return postsList;
    } catch (e) {
      debugPrint(e.toString());
      return null;
    }
  }

  @override
  Future<List<AlbumPhotoDetails>> fetchAlbumsPhotos(int albumId) async {
    ApiClient apiClient = ApiClient.public();
    try {
      Response response = await apiClient.dioClient.get(
        ApiEndPoints.albumPhotos(albumId),
      );

      List<AlbumPhotoDetails> postsList = (response.data as List)?.map((e) => e == null ? null : AlbumPhotoDetails.fromJson(e as Map<String, dynamic>))?.toList();
      debugPrint("albums photos length: ${postsList.length}");
      return postsList;
    } catch (e) {
      debugPrint(e.toString());
      return null;
    }
  }
}

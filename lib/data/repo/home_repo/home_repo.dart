
import 'package:album/data/models/comments/comments_details_dto.dart';
import 'package:album/data/models/posts/index.dart';

abstract class HomeRepo {
  Future<List<PostDetailsDTO>> fetchPostsList();
  Future<void> createPost(String title, String body);
  Future<List<CommentsDetailsDTO>> fetchComments(int postId);
}

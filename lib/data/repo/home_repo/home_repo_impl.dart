import 'package:album/blocs/index.dart';
import 'package:album/constants/index.dart';
import 'package:album/data/local/db/my_db.dart';
import 'package:album/data/models/comments/comments_details_dto.dart';
import 'package:album/data/models/posts/index.dart';
import 'package:album/data/network/index.dart';
import 'package:album/data/repo/home_repo/index.dart';
import 'package:album/utils/index.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';

class HomeRepoImpl extends HomeRepo {
  MyDatabase _myDatabase;

  HomeRepoImpl() {
    _myDatabase = locator.get();
  }

  @override
  Future<List<PostDetailsDTO>> fetchPostsList() async {
    ApiClient apiClient = ApiClient.public();
    try {
      Response response = await apiClient.dioClient.get(
        ApiEndPoints.posts,
      );

      List<PostDetailsDTO> postsList = (response.data as List)?.map((e) => e == null ? null : PostDetailsDTO.fromJson(e as Map<String, dynamic>))?.toList();
      return postsList;
    } catch (e) {
      debugPrint(e.toString());
      return null;
    }
  }

  @override
  Future<void> createPost(String title, String body) async {
    ApiClient apiClient = ApiClient.public();
    try {
      Response response = await apiClient.dioClient.post(
        ApiEndPoints.createPost,
        data: {
          "title": title,
          "body": body,
          "userId": LoginBloc.userId,
        },
      );
      debugPrint(response.data.toString());
    } catch (e) {
      rethrow;
    }
  }

  @override
  Future<List<CommentsDetailsDTO>> fetchComments(int postId) async {
    ApiClient apiClient = ApiClient.public();
    try {
      Response response = await apiClient.dioClient.get(
        ApiEndPoints.viewComments(postId),
      );

      List<CommentsDetailsDTO> commentsList = (response.data as List)?.map((e) => e == null ? null : CommentsDetailsDTO.fromJson(e as Map<String, dynamic>))?.toList();
      debugPrint("commentsList length: ${commentsList.length}");
      return commentsList;
    } catch (e) {
      debugPrint(e.toString());
      return null;
    }
  }
}

import 'package:album/data/models/user/index.dart';

abstract class LoginRepo {
  Future<UserDetailsDTO> verifyUser(String emailId, String password);
  Future<void> fetchUsers();
  UserDetailsDTO getUserDetails();
  int getUserID();
}

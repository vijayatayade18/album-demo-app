import 'dart:convert';

import 'package:album/constants/index.dart';
import 'package:album/data/local/db/my_db.dart';
import 'package:album/data/models/user/user_details_dto.dart';
import 'package:album/data/network/index.dart';
import 'package:album/data/repo/login_repo/index.dart';
import 'package:album/utils/index.dart';
import 'package:album/utils/shared_preferences_handler.dart';
import 'package:dio/dio.dart';
import 'package:flutter/material.dart';

class LoginRepoImpl extends LoginRepo {
  MyDatabase _myDatabase;

  LoginRepoImpl() {
    _myDatabase = locator.get();
  }

  @override
  Future<UserDetailsDTO> verifyUser(String userName, String password) async {
    if (userName != null && password != null) {
      UserDetailsEntityData userDetailsEntityData = await _myDatabase.userDetailsDao.getUserByEmailId(userName);
      if (userDetailsEntityData != null && userName == userDetailsEntityData.email && password == userDetailsEntityData.password) {
        await SharedPreferencesHandler.instance.setString(
          SharedPrefConstants.USER_INFO,
          userDetailsEntityData.content,
        );

        await SharedPreferencesHandler.instance.setInt(
          SharedPrefConstants.USER_ID,
          userDetailsEntityData.id,
        );

        return UserDetailsDTO.fromJson(json.decode(userDetailsEntityData.content));
      }
      return null;
    } else {
      return null;
    }
  }

  @override
  Future<List<UserDetailsDTO>> fetchUsers() async {
    ApiClient apiClient = ApiClient.public();
    try {
      Response response = await apiClient.dioClient.get(
        ApiEndPoints.users,
      );
      List<UserDetailsDTO> userList = (response.data as List)?.map((e) => e == null ? null : UserDetailsDTO.fromJson(e as Map<String, dynamic>))?.toList();

      await _myDatabase.userDetailsDao.insertAllUsers(userList);

      return userList;
    } catch (e) {
      debugPrint(e.toString());
      return null;
    }
  }

  @override
  UserDetailsDTO getUserDetails() {
    String userDetailsJsonString = SharedPreferencesHandler.instance.getString(
      SharedPrefConstants.USER_INFO,
    );
    if (userDetailsJsonString != null) {
      return UserDetailsDTO.fromJson(json.decode(userDetailsJsonString));
    } else {
      return null;
    }
  }

  @override
  int getUserID() {
    return SharedPreferencesHandler.instance.getInt(
      SharedPrefConstants.USER_ID,
    );
  }
}

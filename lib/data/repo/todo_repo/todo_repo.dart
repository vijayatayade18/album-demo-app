import 'package:album/data/models/todo/index.dart';

abstract class TodoRepo {
  Future<List<TodoDetailsDTO>> fetchTodosList();
}

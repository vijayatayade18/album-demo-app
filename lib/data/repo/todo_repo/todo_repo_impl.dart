import 'package:album/constants/index.dart';
import 'package:album/data/local/db/my_db.dart';
import 'package:album/data/models/todo/index.dart';
import 'package:album/data/network/index.dart';
import 'package:album/data/repo/todo_repo/index.dart';
import 'package:album/utils/index.dart';
import 'package:dio/dio.dart';
import 'package:flutter/cupertino.dart';

class TodosRepoImpl extends TodoRepo {
  MyDatabase _myDatabase;

  TodosRepoImpl() {
    _myDatabase = locator.get();
  }

  @override
  Future<List<TodoDetailsDTO>> fetchTodosList() async {
    ApiClient apiClient = ApiClient.public();
    try {
      Response response = await apiClient.dioClient.get(
        ApiEndPoints.todos,
      );

      List<TodoDetailsDTO> postsList = (response.data as List)?.map((e) => e == null ? null : TodoDetailsDTO.fromJson(e as Map<String, dynamic>))?.toList();
      debugPrint("albums photos length: ${postsList.length}");
      return postsList;
    } catch (e) {
      debugPrint(e.toString());
      return null;
    }
  }
}

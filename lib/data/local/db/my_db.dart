import 'package:album/data/local/dao/index.dart';
import 'package:album/data/local/dao/user_details_dao.dart';
import 'package:album/data/local/entities/index.dart';
import 'package:moor/moor.dart';
import 'package:moor_flutter/moor_flutter.dart';

part 'my_db.g.dart';

@UseMoor(
  tables: [
    UserDetailsEntity,
  ],
  daos: [UserDetailsDao],
)
class MyDatabase extends _$MyDatabase {
  MyDatabase()
      : super(FlutterQueryExecutor.inDatabaseFolder(
          path: 'db.sqlite',
        ));

  @override
  int get schemaVersion => 1;
}

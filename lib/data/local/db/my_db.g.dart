// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'my_db.dart';

// **************************************************************************
// MoorGenerator
// **************************************************************************

// ignore_for_file: unnecessary_brace_in_string_interps, unnecessary_this
class UserDetailsEntityData extends DataClass
    implements Insertable<UserDetailsEntityData> {
  final int id;
  final String content;
  final String email;
  final String password;
  UserDetailsEntityData(
      {@required this.id,
      @required this.content,
      @required this.email,
      @required this.password});
  factory UserDetailsEntityData.fromData(
      Map<String, dynamic> data, GeneratedDatabase db,
      {String prefix}) {
    final effectivePrefix = prefix ?? '';
    final intType = db.typeSystem.forDartType<int>();
    final stringType = db.typeSystem.forDartType<String>();
    return UserDetailsEntityData(
      id: intType.mapFromDatabaseResponse(data['${effectivePrefix}id']),
      content:
          stringType.mapFromDatabaseResponse(data['${effectivePrefix}content']),
      email:
          stringType.mapFromDatabaseResponse(data['${effectivePrefix}email']),
      password: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}password']),
    );
  }
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (!nullToAbsent || id != null) {
      map['id'] = Variable<int>(id);
    }
    if (!nullToAbsent || content != null) {
      map['content'] = Variable<String>(content);
    }
    if (!nullToAbsent || email != null) {
      map['email'] = Variable<String>(email);
    }
    if (!nullToAbsent || password != null) {
      map['password'] = Variable<String>(password);
    }
    return map;
  }

  UserDetailsEntityCompanion toCompanion(bool nullToAbsent) {
    return UserDetailsEntityCompanion(
      id: id == null && nullToAbsent ? const Value.absent() : Value(id),
      content: content == null && nullToAbsent
          ? const Value.absent()
          : Value(content),
      email:
          email == null && nullToAbsent ? const Value.absent() : Value(email),
      password: password == null && nullToAbsent
          ? const Value.absent()
          : Value(password),
    );
  }

  factory UserDetailsEntityData.fromJson(Map<String, dynamic> json,
      {ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return UserDetailsEntityData(
      id: serializer.fromJson<int>(json['id']),
      content: serializer.fromJson<String>(json['content']),
      email: serializer.fromJson<String>(json['email']),
      password: serializer.fromJson<String>(json['password']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'content': serializer.toJson<String>(content),
      'email': serializer.toJson<String>(email),
      'password': serializer.toJson<String>(password),
    };
  }

  UserDetailsEntityData copyWith(
          {int id, String content, String email, String password}) =>
      UserDetailsEntityData(
        id: id ?? this.id,
        content: content ?? this.content,
        email: email ?? this.email,
        password: password ?? this.password,
      );
  @override
  String toString() {
    return (StringBuffer('UserDetailsEntityData(')
          ..write('id: $id, ')
          ..write('content: $content, ')
          ..write('email: $email, ')
          ..write('password: $password')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => $mrjf($mrjc(id.hashCode,
      $mrjc(content.hashCode, $mrjc(email.hashCode, password.hashCode))));
  @override
  bool operator ==(dynamic other) =>
      identical(this, other) ||
      (other is UserDetailsEntityData &&
          other.id == this.id &&
          other.content == this.content &&
          other.email == this.email &&
          other.password == this.password);
}

class UserDetailsEntityCompanion
    extends UpdateCompanion<UserDetailsEntityData> {
  final Value<int> id;
  final Value<String> content;
  final Value<String> email;
  final Value<String> password;
  const UserDetailsEntityCompanion({
    this.id = const Value.absent(),
    this.content = const Value.absent(),
    this.email = const Value.absent(),
    this.password = const Value.absent(),
  });
  UserDetailsEntityCompanion.insert({
    this.id = const Value.absent(),
    @required String content,
    @required String email,
    @required String password,
  })  : content = Value(content),
        email = Value(email),
        password = Value(password);
  static Insertable<UserDetailsEntityData> custom({
    Expression<int> id,
    Expression<String> content,
    Expression<String> email,
    Expression<String> password,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (content != null) 'content': content,
      if (email != null) 'email': email,
      if (password != null) 'password': password,
    });
  }

  UserDetailsEntityCompanion copyWith(
      {Value<int> id,
      Value<String> content,
      Value<String> email,
      Value<String> password}) {
    return UserDetailsEntityCompanion(
      id: id ?? this.id,
      content: content ?? this.content,
      email: email ?? this.email,
      password: password ?? this.password,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (content.present) {
      map['content'] = Variable<String>(content.value);
    }
    if (email.present) {
      map['email'] = Variable<String>(email.value);
    }
    if (password.present) {
      map['password'] = Variable<String>(password.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('UserDetailsEntityCompanion(')
          ..write('id: $id, ')
          ..write('content: $content, ')
          ..write('email: $email, ')
          ..write('password: $password')
          ..write(')'))
        .toString();
  }
}

class $UserDetailsEntityTable extends UserDetailsEntity
    with TableInfo<$UserDetailsEntityTable, UserDetailsEntityData> {
  final GeneratedDatabase _db;
  final String _alias;
  $UserDetailsEntityTable(this._db, [this._alias]);
  final VerificationMeta _idMeta = const VerificationMeta('id');
  GeneratedIntColumn _id;
  @override
  GeneratedIntColumn get id => _id ??= _constructId();
  GeneratedIntColumn _constructId() {
    return GeneratedIntColumn('id', $tableName, false,
        $customConstraints: 'UNIQUE');
  }

  final VerificationMeta _contentMeta = const VerificationMeta('content');
  GeneratedTextColumn _content;
  @override
  GeneratedTextColumn get content => _content ??= _constructContent();
  GeneratedTextColumn _constructContent() {
    return GeneratedTextColumn(
      'content',
      $tableName,
      false,
    );
  }

  final VerificationMeta _emailMeta = const VerificationMeta('email');
  GeneratedTextColumn _email;
  @override
  GeneratedTextColumn get email => _email ??= _constructEmail();
  GeneratedTextColumn _constructEmail() {
    return GeneratedTextColumn(
      'email',
      $tableName,
      false,
    );
  }

  final VerificationMeta _passwordMeta = const VerificationMeta('password');
  GeneratedTextColumn _password;
  @override
  GeneratedTextColumn get password => _password ??= _constructPassword();
  GeneratedTextColumn _constructPassword() {
    return GeneratedTextColumn(
      'password',
      $tableName,
      false,
    );
  }

  @override
  List<GeneratedColumn> get $columns => [id, content, email, password];
  @override
  $UserDetailsEntityTable get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'user_details_entity';
  @override
  final String actualTableName = 'user_details_entity';
  @override
  VerificationContext validateIntegrity(
      Insertable<UserDetailsEntityData> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id'], _idMeta));
    }
    if (data.containsKey('content')) {
      context.handle(_contentMeta,
          content.isAcceptableOrUnknown(data['content'], _contentMeta));
    } else if (isInserting) {
      context.missing(_contentMeta);
    }
    if (data.containsKey('email')) {
      context.handle(
          _emailMeta, email.isAcceptableOrUnknown(data['email'], _emailMeta));
    } else if (isInserting) {
      context.missing(_emailMeta);
    }
    if (data.containsKey('password')) {
      context.handle(_passwordMeta,
          password.isAcceptableOrUnknown(data['password'], _passwordMeta));
    } else if (isInserting) {
      context.missing(_passwordMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  UserDetailsEntityData map(Map<String, dynamic> data, {String tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return UserDetailsEntityData.fromData(data, _db, prefix: effectivePrefix);
  }

  @override
  $UserDetailsEntityTable createAlias(String alias) {
    return $UserDetailsEntityTable(_db, alias);
  }
}

abstract class _$MyDatabase extends GeneratedDatabase {
  _$MyDatabase(QueryExecutor e) : super(SqlTypeSystem.defaultInstance, e);
  $UserDetailsEntityTable _userDetailsEntity;
  $UserDetailsEntityTable get userDetailsEntity =>
      _userDetailsEntity ??= $UserDetailsEntityTable(this);
  UserDetailsDao _userDetailsDao;
  UserDetailsDao get userDetailsDao =>
      _userDetailsDao ??= UserDetailsDao(this as MyDatabase);
  @override
  Iterable<TableInfo> get allTables => allSchemaEntities.whereType<TableInfo>();
  @override
  List<DatabaseSchemaEntity> get allSchemaEntities => [userDetailsEntity];
}

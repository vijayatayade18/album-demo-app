import 'package:moor_flutter/moor_flutter.dart';

class UserDetailsEntity extends Table {
  IntColumn get id => integer().customConstraint("UNIQUE")();

  TextColumn get content => text()();

  TextColumn get email => text()();

  TextColumn get password => text()();


  @override
  Set<Column> get primaryKey => {id};
}

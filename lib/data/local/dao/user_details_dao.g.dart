// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'user_details_dao.dart';

// **************************************************************************
// DaoGenerator
// **************************************************************************

mixin _$UserDetailsDaoMixin on DatabaseAccessor<MyDatabase> {
  $UserDetailsEntityTable get userDetailsEntity =>
      attachedDatabase.userDetailsEntity;
}

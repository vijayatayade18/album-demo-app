import 'dart:convert';

import 'package:album/data/local/db/my_db.dart';
import 'package:album/data/local/entities/index.dart';
import 'package:album/data/models/user/index.dart';
import 'package:moor_flutter/moor_flutter.dart';

part 'user_details_dao.g.dart';

@UseDao(tables: [UserDetailsEntity])
class UserDetailsDao extends DatabaseAccessor<MyDatabase> with _$UserDetailsDaoMixin {
  UserDetailsDao(MyDatabase db) : super(db);

  Future<List<UserDetailsEntityData>> get userList => select(userDetailsEntity).get();

  Future insertUserDetails(UserDetailsEntityData userDetailsEntityData) => into(userDetailsEntity).insertOnConflictUpdate(userDetailsEntityData);

  Future updateUser(UserDetailsEntityData userDetailsEntityData) => update(userDetailsEntity).replace(userDetailsEntityData);

  Future<UserDetailsEntityData> getUserById(int id) => (select(userDetailsEntity)..where((t) => t.id.equals(id))).getSingle();

  Future<void> insertAllUsers(List<UserDetailsDTO> list) async {
    List<UserDetailsEntityData> usersList = list.map((e) {
      if (e != null) {
        return UserDetailsEntityData(
          id: e.id,
          email: e.email,
          password: e.username,
          content: json.encode(e),
        );
      }
    }).toList();

    await batch((batch) {
      batch.insertAll(
        userDetailsEntity,
        usersList,
        mode: InsertMode.insertOrReplace,
      );
    });
  }

  Future<UserDetailsEntityData> getUserByEmailId(String username) => (select(userDetailsEntity)..where((t) => t.email.equals(username))).getSingle();
}

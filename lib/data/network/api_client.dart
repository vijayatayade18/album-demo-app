import 'package:album/constants/api_endpoints.dart';
import 'package:dio/dio.dart';

const int NETWORK_TIMEOUT = 900000;

class ApiClient {
  Dio dioClient;

  //A client to directly make Public Calls
  ApiClient.public({
    BaseOptions baseOptions,
  }) {
    if (baseOptions == null) {
      baseOptions = BaseOptions(
        connectTimeout: NETWORK_TIMEOUT,
        receiveTimeout: NETWORK_TIMEOUT,
        followRedirects: true,
        baseUrl: ApiEndPoints.baseUrl
      );
    }
    dioClient = Dio(baseOptions);
  }
}
